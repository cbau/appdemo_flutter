# AppDemo

Welcome to AppDemo.

This app works as a showcase for some of the most common features on developed apps.

## Features

- Multiplatform responsiveness.
- Localization on multiple languages.
- Rest API consuming.
- Local persistence.
- Field validation.
- Firebase Analytics & Crashlytics integration.
- Firebase Cloud Messaging integration for push notifications.
- Firebase Cloud Firestore data consuming.

## Releases

See this project working on web at this link:

[WebApp](https://cbau.gitlab.io/appdemo_flutter/)

To download the latest version, visit the releases page:

[Releases](https://gitlab.com/cbau/appdemo_flutter/-/releases)

## Run on your device

To run this project, first install Flutter from [flutter.dev](https://flutter.dev).

For first time configuration, you must get the dependencies with the command below:

```bash
flutter pub get
```

If the folder `gen` in `lib/l10n` is missing or outdated, run this command to generate it:

```bash
flutter gen-l10n
```

Then, use this command to run the project on any of your available devices:

```bash
flutter run --flavor prod --dart-define flavor=prod --release
```

Or use this command to run in development mode:

```bash
flutter run --flavor dev --dart-define flavor=dev
```

## Dependencies

Here is a summarized information of the dependencies and their function in this project:

**Cached network image**. Retrieves and manages cache for images downloaded from the network.

**Cloud Firestore**. Retrieves data from Firebase's Cloud Firestore.

**Firebase Analytics**. Sends app usage to Firebase for analysis.

**Firebase Core**. Integrates the app with Firebase.

**Firebase Crashlytics**. Sends app crashes to Firebase for tracking and fixing.

**Firebase Messaging**. Allows the app to receive push notifications from Firebase.

**Flutter bloc**. Manage the state on Flutter widgets.

**Flutter local notifications**. Allows sending local notifications to the device.

**Flutter localizations**. Translates the default widgets to the device locale.

**Go router**. Allows to move through the screens with web routing support.

**Http**. Manages the requests from the network.

**Intl**. Supports internationalization for text translations and date formatting.

**Modal bottom sheet**. Improved modal bottom sheet.

**Package info plus**. Retrieves package info among all platforms.

**Path provider**. Retrieves available paths on the current platform for file management.

**Sembast**. Manages the local database.

**Share plus**. Manages sharing easier among all platforms.

## Development dependencies

**Flutter lints**. Checks the code for common mistakes and optimizations.

**Flutter test**. Allows the creation of automated tests for this project.
