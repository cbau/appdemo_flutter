import 'dart:async';

import 'package:flutter/services.dart';
import 'package:flutter_test/flutter_test.dart';

Future<void> testExecutable(FutureOr<void> Function() testMain) async {
  setUpAll(
    () async {
      final fonts = [
        const _FontReference(
          name: 'MaterialIcons',
          path: 'fonts/MaterialIcons-Regular.otf',
        ),
        const _FontReference(
          name: 'Roboto',
          path: 'fonts/Roboto-Regular.ttf',
        ),
      ];

      for (final font in fonts) {
        final fontData = rootBundle.load(font.path);
        final fontLoader = FontLoader(font.name)..addFont(fontData);
        await fontLoader.load();
      }
    },
  );

  await testMain();
}

class _FontReference {
  const _FontReference({
    required this.name,
    required this.path,
  });

  final String name;
  final String path;
}
