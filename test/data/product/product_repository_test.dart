import 'package:appdemo_flutter/data/common/fake_store_service.dart';
import 'package:appdemo_flutter/data/common/local_service.dart';
import 'package:appdemo_flutter/data/product/product.dart';
import 'package:appdemo_flutter/data/product/product_repository.dart';
import 'package:flutter_test/flutter_test.dart';

import '../common/test_client.dart';

void main() {
  setUpAll(
    () {
      LocalService().openDatabaseFromMemory = true;
      FakeStoreService().setClient(TestClient());
    },
  );

  test(
    'When all the products are forced requested in repository, '
    'and there is no local data, '
    'then emit a list of products once.',
    () async {
      final stream = ProductRepository().getAll(force: true);

      expect(
        stream,
        emitsInOrder(
          [
            isA<List<Product>>(),
          ],
        ),
      );
    },
  );

  test(
    'When all the products are forced requested in repository, '
    'and there is local data, '
    'then emit a list of products twice.',
    () async {
      final stream = ProductRepository().getAll(force: true);

      expect(
        stream,
        emitsInOrder(
          [
            isA<List<Product>>(),
            isA<List<Product>>(),
          ],
        ),
      );
    },
  );

  test(
    'When all the products are requested but not forced in repository, '
    'and there is local data, '
    'then emit a list of products once.',
    () async {
      final stream = ProductRepository().getAll();

      expect(
        stream,
        emitsInOrder(
          [
            isA<List<Product>>(),
          ],
        ),
      );
    },
  );
}
