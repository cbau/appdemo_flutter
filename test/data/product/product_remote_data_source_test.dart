import 'package:appdemo_flutter/data/common/fake_store_service.dart';
import 'package:appdemo_flutter/data/common/http_status_exception.dart';
import 'package:appdemo_flutter/data/product/product.dart';
import 'package:appdemo_flutter/data/product/product_remote_data_source.dart';
import 'package:appdemo_flutter/data/product/rating.dart';
import 'package:flutter_test/flutter_test.dart';

import '../common/test_client.dart';

void main() {
  setUpAll(
    () => FakeStoreService().setClient(TestClient()),
  );

  test(
    'When all the products are requested in remote data source, '
    'then return a list of products.',
    () async {
      final products = await ProductRemoteDataSource().all;

      expect(products, isA<List<Product>>());
      expect(products.isEmpty, false);
      expect(products[0].id, 1);
      expect(products[0].category, "men's clothing");
      expect(products[0].price, 109.95);
      expect(products[0].rating, isA<Rating>());
      expect(products[0].rating.rate, 3.9);
      expect(products[0].rating.count, 120);
    },
  );

  test(
    'When an existing product is requested in remote data source, '
    'then return the product.',
    () async {
      final product = await ProductRemoteDataSource().getById(1);

      expect(product, isA<Product>());
      expect(product.id, 1);
      expect(product.category, "men's clothing");
      expect(product.price, 109.95);
      expect(product.rating, isA<Rating>());
      expect(product.rating.rate, 3.9);
      expect(product.rating.count, 120);
    },
  );

  test(
    'When an unexistent product is requested in remote data source, '
    'then return an error.',
    () async {
      expect(
        ProductRemoteDataSource().getById(0),
        throwsA(isA<HttpStatusException>()),
      );
    },
  );
}
