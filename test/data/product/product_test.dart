import 'package:appdemo_flutter/data/product/product.dart';
import 'package:flutter_test/flutter_test.dart';

import '../common/test_data.dart';

void main() {
  test(
    'When a product is created from a valid map, '
    "then return a product with it's values.",
    () {
      final product = Product.fromMap(const {
        'id': 1,
        'title': 'Fjallraven - Foldsack No. 1 Backpack, Fits 15 Laptops',
        'price': 109.95,
        'description': 'Your perfect pack for everyday use and walks in the '
            'forest. Stash your laptop (up to 15 inches) in the padded sleeve, '
            'your everyday',
        'category': "men's clothing",
        'image': 'https://fakestoreapi.com/img/81fPKd-2AYL._AC_SL1500_.jpg',
        'rating': {
          'rate': 3.9,
          'count': 120,
        },
      });

      final matchProduct = TestData().product;

      expect(product, isA<Product>());
      expect(product, matchProduct);
      expect(product.hashCode, matchProduct.hashCode);
      expect(product.toString(), isA<String>());
    },
  );

  test(
    'When a product is created from a null map, '
    'then return a product with default values.',
    () {
      final product = Product.fromMap(null);

      const matchProduct = Product(
        id: 0,
        url: 'https://cbau.gitlab.io/appdemo_flutter/products/0',
      );

      expect(product, isA<Product>());
      expect(product, matchProduct);
    },
  );

  test(
    'When a product is created from an empty map, '
    'then return a product with default values.',
    () {
      final product = Product.fromMap(const {});

      const matchProduct = Product(
        id: 0,
        url: 'https://cbau.gitlab.io/appdemo_flutter/products/0',
      );

      expect(product, isA<Product>());
      expect(product, matchProduct);
    },
  );

  test(
    'When a product is created from a map with null values, '
    'then return a product with default values.',
    () {
      final product = Product.fromMap(const {
        'id': null,
        'title': null,
        'price': null,
        'description': null,
        'category': null,
        'image': null,
        'rating': {
          'rate': null,
          'count': null,
        },
      });

      const matchProduct = Product(
        id: 0,
        url: 'https://cbau.gitlab.io/appdemo_flutter/products/0',
      );

      expect(product, isA<Product>());
      expect(product, matchProduct);
    },
  );
}
