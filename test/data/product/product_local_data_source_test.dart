import 'package:appdemo_flutter/data/common/local_service.dart';
import 'package:appdemo_flutter/data/product/product.dart';
import 'package:appdemo_flutter/data/product/product_local_data_source.dart';
import 'package:flutter_test/flutter_test.dart';

import '../common/test_data.dart';

void main() {
  setUpAll(
    () => LocalService().openDatabaseFromMemory = true,
  );

  tearDown(
    () async => ProductLocalDataSource().clear(),
  );

  tearDownAll(
    () async => LocalService().clearAll(),
  );

  test(
    'When all the products are requested in local data source, '
    'then return an empty list.',
    () async {
      final products = await ProductLocalDataSource().all;

      expect(products, isA<List<Product>>());
      expect(products.isEmpty, true);
    },
  );

  test(
    'When a product is added in local data source, '
    'then you can retrieve it by id.',
    () async {
      final products = await ProductLocalDataSource().all;
      expect(products, isA<List<Product>>());
      expect(products.isEmpty, true);

      final product = TestData().product;

      await ProductLocalDataSource().put(product);

      final productById = await ProductLocalDataSource().getById(product.id);

      expect(productById, product);
    },
  );

  test(
    'When a product is edited in local data source, '
    'then all products will contain the new item without duplication.',
    () async {
      var products = await ProductLocalDataSource().all;
      expect(products, isA<List<Product>>());
      expect(products.isEmpty, true);

      final product = TestData().product;

      await ProductLocalDataSource().put(product);

      products = await ProductLocalDataSource().all;

      expect(products.length, 1);
      expect(products.last, product);

      final newProduct = product.copyWith(
        title: 'New title product',
      );

      await ProductLocalDataSource().put(newProduct);

      products = await ProductLocalDataSource().all;

      expect(products.length, 1);
      expect(products.last, newProduct);
      expect(products.last.id, product.id);
    },
  );

  test(
    'When a product is deleted in local data source, '
    'then all products will not contain it anymore.',
    () async {
      var products = await ProductLocalDataSource().all;
      expect(products, isA<List<Product>>());
      expect(products.isEmpty, true);

      final product = TestData().product;

      await ProductLocalDataSource().put(product);

      products = await ProductLocalDataSource().all;

      expect(products.length, 1);
      expect(products.last, product);

      await ProductLocalDataSource().deleteById(product.id);

      products = await ProductLocalDataSource().all;

      expect(products.isEmpty, true);
    },
  );
}
