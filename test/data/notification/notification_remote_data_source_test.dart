import 'package:appdemo_flutter/data/notification/notification.dart';
import 'package:appdemo_flutter/data/notification/notification_remote_data_source.dart';
import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:fake_cloud_firestore/fake_cloud_firestore.dart';
import 'package:flutter_test/flutter_test.dart';

void main() {
  setUpAll(
    () async {
      final fakeFirestore = FakeFirebaseFirestore();
      await fakeFirestore.collection('notifications').add({
        'title': 'Notification Test',
        'message': 'This is a notification test.',
        'timestamp': Timestamp.fromDate(DateTime(2001)),
      });
      NotificationRemoteDataSource().firestore = fakeFirestore;
    },
  );

  test(
    'When all the notifications are requested in remote data source, '
    'then return a list of notifications.',
    () async {
      final notifications = await NotificationRemoteDataSource().all;

      expect(notifications, isA<List<Notification>>());
      expect(notifications.isEmpty, false);
      expect(notifications[0].title, 'Notification Test');
      expect(notifications[0].message, 'This is a notification test.');
      expect(notifications[0].timestamp, DateTime(2001));
    },
  );
}
