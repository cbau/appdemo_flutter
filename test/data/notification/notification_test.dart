import 'package:appdemo_flutter/data/notification/notification.dart';
import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:flutter_test/flutter_test.dart';

import '../common/test_data.dart';

void main() {
  test(
    'When a notification is created from a valid map, '
    "then return a notification with it's values.",
    () {
      final notification = Notification.fromMap({
        'title': 'Notification Test',
        'message': 'This is a notification test.',
        'timestamp': Timestamp.fromDate(DateTime(2001)),
      });

      final matchNotification = TestData().notification;

      expect(notification, isA<Notification>());
      expect(notification.message, matchNotification.message);
      expect(notification.timestamp, matchNotification.timestamp);
      expect(notification.title, matchNotification.title);
      expect(notification.hashCode == matchNotification.hashCode, false);
      expect(notification.toString(), isA<String>());
    },
  );

  test(
    'When a notification is created from a null map, '
    'then return a notification with default values.',
    () {
      final notification = Notification.fromMap(null);

      final matchNotification = Notification(
        message: '',
        timestamp: DateTime.fromMillisecondsSinceEpoch(0),
        title: '',
      );

      expect(notification, isA<Notification>());
      expect(notification.message, matchNotification.message);
      expect(notification.timestamp, matchNotification.timestamp);
      expect(notification.title, matchNotification.title);
    },
  );

  test(
    'When a notification is created from an empty map, '
    'then return a notification with default values.',
    () {
      final notification = Notification.fromMap(const {});

      final matchNotification = Notification(
        message: '',
        timestamp: DateTime.fromMillisecondsSinceEpoch(0),
        title: '',
      );

      expect(notification, isA<Notification>());
      expect(notification.message, matchNotification.message);
      expect(notification.timestamp, matchNotification.timestamp);
      expect(notification.title, matchNotification.title);
    },
  );

  test(
    'When a notification is created from a map with null values, '
    'then return a notification with default values.',
    () {
      final notification = Notification.fromMap({
        'title': null,
        'message': null,
        'timestamp': null,
      });

      final matchNotification = Notification(
        message: '',
        timestamp: DateTime.fromMillisecondsSinceEpoch(0),
        title: '',
      );

      expect(notification, isA<Notification>());
      expect(notification.message, matchNotification.message);
      expect(notification.timestamp, matchNotification.timestamp);
      expect(notification.title, matchNotification.title);
    },
  );
}
