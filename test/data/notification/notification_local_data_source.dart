import 'package:appdemo_flutter/data/common/local_service.dart';
import 'package:appdemo_flutter/data/notification/notification.dart';
import 'package:appdemo_flutter/data/notification/notification_local_data_source.dart';
import 'package:flutter_test/flutter_test.dart';

import '../common/test_data.dart';

void main() {
  setUpAll(
    () => LocalService().openDatabaseFromMemory = true,
  );

  tearDown(
    () async => NotificationLocalDataSource().clear(),
  );

  tearDownAll(
    () async => LocalService().clearAll(),
  );

  test(
    'When all the notifications are requested in local data source, '
    'then return an empty list.',
    () async {
      final notifications = await NotificationLocalDataSource().all;

      expect(notifications, isA<List<Notification>>());
      expect(notifications.isEmpty, true);
    },
  );

  test(
    'When a notification is added in local data source, '
    'then it appears in the list.',
    () async {
      final notifications = await NotificationLocalDataSource().all;
      expect(notifications, isA<List<Notification>>());
      expect(notifications.isEmpty, true);

      final notification = TestData().notification;

      await NotificationLocalDataSource().add(notification);

      final newNotifications = await NotificationLocalDataSource().all;

      expect(newNotifications.last, notification);
    },
  );
}
