import 'package:appdemo_flutter/data/common/local_service.dart';
import 'package:appdemo_flutter/data/notification/notification.dart';
import 'package:appdemo_flutter/data/notification/notification_remote_data_source.dart';
import 'package:appdemo_flutter/data/notification/notification_repository.dart';
import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:fake_cloud_firestore/fake_cloud_firestore.dart';
import 'package:flutter_test/flutter_test.dart';

void main() {
  setUpAll(
    () async {
      LocalService().openDatabaseFromMemory = true;
      final fakeFirestore = FakeFirebaseFirestore();
      await fakeFirestore.collection('notifications').add({
        'title': 'Notification Test',
        'message': 'This is a notification test.',
        'timestamp': Timestamp.fromDate(DateTime(2001)),
      });
      NotificationRemoteDataSource().firestore = fakeFirestore;
    },
  );

  test(
    'When all the notifications are forced requested in repository, '
    'and there is no local data, '
    'then emit a list of notifications once.',
    () async {
      final stream = NotificationRepository().getAll(force: true);

      expect(
        stream,
        emitsInOrder(
          [
            isA<List<Notification>>(),
          ],
        ),
      );
    },
  );

  test(
    'When all the notifications are forced requested in repository, '
    'and there is local data, '
    'then emit a list of notifications twice.',
    () async {
      final stream = NotificationRepository().getAll(force: true);

      expect(
        stream,
        emitsInOrder(
          [
            isA<List<Notification>>(),
            isA<List<Notification>>(),
          ],
        ),
      );
    },
  );

  test(
    'When all the notifications are requested but not forced in repository, '
    'and there is local data, '
    'then emit a list of notifications once.',
    () async {
      final stream = NotificationRepository().getAll();

      expect(
        stream,
        emitsInOrder(
          [
            isA<List<Notification>>(),
          ],
        ),
      );
    },
  );
}
