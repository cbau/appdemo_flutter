import 'package:appdemo_flutter/data/notification/notification.dart';
import 'package:appdemo_flutter/data/product/product.dart';
import 'package:appdemo_flutter/data/product/rating.dart';

class TestData {
  factory TestData() => _instance;

  const TestData._();

  static const _instance = TestData._();

  Notification get notification => Notification(
        title: 'Notification Test',
        message: 'This is a notification test.',
        timestamp: DateTime(2001),
      );

  Product get product => const Product(
        id: 1,
        title: 'Fjallraven - Foldsack No. 1 Backpack, Fits 15 Laptops',
        price: 109.95,
        description: 'Your perfect pack for everyday use and walks in the '
            'forest. Stash your laptop (up to 15 inches) in the padded sleeve, '
            'your everyday',
        category: "men's clothing",
        image: 'https://fakestoreapi.com/img/81fPKd-2AYL._AC_SL1500_.jpg',
        url: 'https://cbau.gitlab.io/appdemo_flutter/products/1',
        rating: Rating(
          rate: 3.9,
          count: 120,
        ),
      );
}
