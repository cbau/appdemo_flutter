import 'package:appdemo_flutter/extensions.dart';
import 'package:flutter_test/flutter_test.dart';

void main() {
  group(
    'When string is validated as email,',
    () {
      test(
        'and string is empty, '
        'then email is invalid.',
        () {
          expect(''.isValidEmail, false);
        },
      );

      test(
        'and string is text (jsmith), '
        'then email is invalid.',
        () {
          expect('jsmith'.isValidEmail, false);
        },
      );

      test(
        'and string has an empty domain (jsmith@), '
        'then email is invalid.',
        () {
          expect('jsmith@'.isValidEmail, false);
        },
      );

      test(
        'and string has an incomplete subdomain (jsmith@example.), '
        'then email is invalid.',
        () {
          expect('jsmith@example.'.isValidEmail, false);
        },
      );

      test(
        'and string has an incomplete country domain (jsmith@example.co.), '
        'then email is invalid.',
        () {
          expect('jsmith@example.co.'.isValidEmail, false);
        },
      );

      test(
        'and string has an incomplete subdomain (jsmith@.example.co.uk), '
        'then email is invalid.',
        () {
          expect('jsmith@.example.co.uk'.isValidEmail, false);
        },
      );

      test(
        "and string has underscores on it's domain (jsmith@example_one.com), "
        'then email is invalid.',
        () {
          expect('jsmith@example_one.com'.isValidEmail, false);
        },
      );

      test(
        'and string has a simple domain (jsmith@example), '
        'then email is valid.',
        () {
          expect('jsmith@example'.isValidEmail, true);
        },
      );

      test(
        'and string has a subdomain (jsmith@example.com), '
        'then email is valid.',
        () {
          expect('jsmith@example.com'.isValidEmail, true);
        },
      );

      test(
        'and string has a country domain (jsmith@example.co.uk), '
        'then email is valid.',
        () {
          expect('jsmith@example.co.uk'.isValidEmail, true);
        },
      );

      test(
        'and string has a complex domain (jsmith@mail.example.co.uk), '
        'then email is valid.',
        () {
          expect('jsmith@mail.example.co.uk'.isValidEmail, true);
        },
      );

      test(
        'and string has an alias (jsmith+alias@example.com), '
        'then email is valid.',
        () {
          expect('jsmith+alias@example.com'.isValidEmail, true);
        },
      );

      test(
        'and string has an empty alias (jsmith+@example.com), '
        'then email is valid.',
        () {
          expect('jsmith+@example.com'.isValidEmail, true);
        },
      );

      test(
        "and string has dashes on it's domain (jsmith@example-one.com), "
        'then email is valid.',
        () {
          expect('jsmith@example-one.com'.isValidEmail, true);
        },
      );

      test(
        "and string has dashes on it's name (j-smith@example.com), "
        'then email is valid.',
        () {
          expect('j-smith@example.com'.isValidEmail, true);
        },
      );

      test(
        "and string has dots on it's name (j.smith@example.com), "
        'then email is valid.',
        () {
          expect('j.smith@example.com'.isValidEmail, true);
        },
      );

      test(
        "and string has underscores on it's name (j_smith@example.com), "
        'then email is valid.',
        () {
          expect('j_smith@example.com'.isValidEmail, true);
        },
      );
    },
  );
}
