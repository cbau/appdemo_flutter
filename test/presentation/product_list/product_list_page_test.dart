import 'package:appdemo_flutter/data/common/fake_store_service.dart';
import 'package:appdemo_flutter/data/common/local_service.dart';
import 'package:appdemo_flutter/presentation/common/app_network_image.dart';
import 'package:appdemo_flutter/presentation/common/error_dialog.dart';
import 'package:appdemo_flutter/presentation/navigation/navigation_page.dart';
import 'package:appdemo_flutter/presentation/product_list/product_list_item.dart';
import 'package:appdemo_flutter/presentation/product_list/product_list_page.dart';
import 'package:flutter_test/flutter_test.dart';

import '../../data/common/test_client.dart';
import '../../data/common/test_data.dart';
import '../test_app.dart';

void main() {
  setUpAll(
    () {
      AppNetworkImage.isTestMode = true;
      LocalService().openDatabaseFromMemory = true;
    },
  );

  testWidgets(
    'When product list page is shown, '
    'and request is not successful '
    'then display error message.',
    (tester) async {
      FakeStoreService().setClient(TestClient.error());

      await tester.pumpWidget(
        TestApp(
          home: NavigationPage(
            selectedIndex: 1,
            child: ProductListPage(),
          ),
        ),
      );

      // Wait until the bloc data has been loaded.
      await tester.pumpAndSettle();

      final dialogFinder = find.byType(ErrorDialog);

      expect(dialogFinder, findsOne);
    },
  );

  testWidgets(
    'When product list page is shown, '
    'and request is successful '
    'then display as expected.',
    (tester) async {
      FakeStoreService().setClient(TestClient());

      final product = TestData().product;

      await tester.pumpWidget(
        TestApp(
          home: NavigationPage(
            selectedIndex: 1,
            child: ProductListPage(),
          ),
        ),
      );

      // Wait until the bloc data has been loaded.
      await tester.pumpAndSettle();

      final itemFinder = find.byType(ProductListItem);
      final titleFinder = find.text(product.title);

      expect(itemFinder, findsAny);
      expect(titleFinder, findsOne);
    },
  );
}
