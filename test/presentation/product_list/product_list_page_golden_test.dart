import 'package:appdemo_flutter/data/common/fake_store_service.dart';
import 'package:appdemo_flutter/data/common/local_service.dart';
import 'package:appdemo_flutter/l10n/gen/app_localizations.g.dart';
import 'package:appdemo_flutter/presentation/common/app_network_image.dart';
import 'package:appdemo_flutter/presentation/navigation/navigation_page.dart';
import 'package:appdemo_flutter/presentation/product_list/product_list_page.dart';
import 'package:flutter_test/flutter_test.dart';

import '../../data/common/test_client.dart';
import '../test_app.dart';
import '../test_devices.dart';

void main() {
  setUpAll(
    () async {
      AppNetworkImage.isTestMode = true;
      LocalService().openDatabaseFromMemory = true;
    },
  );

  for (final locale in AppLocalizations.supportedLocales) {
    testWidgets(
      'When product list page is shown, '
      'and request is not successful '
      'verify it matches the golden tests.',
      (tester) async {
        FakeStoreService().setClient(TestClient.error());

        await tester.pumpWidget(
          TestApp(
            home: NavigationPage(
              selectedIndex: 1,
              child: ProductListPage(),
            ),
            locale: locale,
          ),
        );

        await TestDevices().matchesGoldenFiles(
          tester,
          'product_list_${locale.languageCode}_error',
        );
      },
    );

    testWidgets(
      'When product list page is shown, '
      'and request is successful '
      'verify it matches the golden tests.',
      (tester) async {
        FakeStoreService().setClient(TestClient());

        await tester.pumpWidget(
          TestApp(
            home: NavigationPage(
              selectedIndex: 1,
              child: ProductListPage(),
            ),
            locale: locale,
          ),
        );

        await TestDevices().matchesGoldenFiles(
          tester,
          'product_list_${locale.languageCode}',
        );
      },
    );
  }
}
