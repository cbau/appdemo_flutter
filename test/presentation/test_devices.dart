import 'dart:ui';

import 'package:flutter_test/flutter_test.dart';

class TestDevices {
  factory TestDevices() => _instance;

  const TestDevices._();

  static const _instance = TestDevices._();

  List<Device> get all => [
        iphoneMax,
        smallPhone,
        smallPhone.dark(),
        smallPhone.copyWith(
          textScale: 0.8,
          name: '${smallPhone.name}_min_text',
        ),
        smallPhone.copyWith(
          textScale: 2,
          name: '${smallPhone.name}_max_text',
        ),
        tabletLandscape,
      ];

  Device get smallPhone => const Device(
        size: Size(360, 640),
        name: 'small_phone',
      );

  Device get iphoneMax => const Device(
        name: 'iphone_max',
        devicePixelRatio: 3,
        safeArea: FakeViewPadding(top: 47, bottom: 34),
        size: Size(428, 926),
      );

  Device get tabletLandscape => const Device(
        size: Size(1024, 768),
        name: 'tablet_landscape',
      );

  Future<void> matchesGoldenFiles(
    WidgetTester tester,
    String name, {
    Future<void> Function(WidgetTester tester)? customPump,
  }) async {
    for (final device in all) {
      tester.platformDispatcher.platformBrightnessTestValue = device.brightness;
      tester.platformDispatcher.textScaleFactorTestValue = device.textScale;
      tester.view.devicePixelRatio = device.devicePixelRatio;
      tester.view.padding = FakeViewPadding(
        bottom: device.safeArea.bottom * device.devicePixelRatio,
        left: device.safeArea.left * device.devicePixelRatio,
        right: device.safeArea.right * device.devicePixelRatio,
        top: device.safeArea.top * device.devicePixelRatio,
      );
      tester.view.physicalSize = device.size * device.devicePixelRatio;

      if (customPump == null) {
        await tester.pumpAndSettle();
      } else {
        await customPump(tester);
      }

      final actual = find.byWidgetPredicate((w) => true).first;
      await expectLater(
        actual,
        matchesGoldenFile('goldens/$name.${device.name}.png'),
      );
    }
  }
}

class Device {
  const Device({
    required this.name,
    required this.size,
    this.brightness = Brightness.light,
    this.devicePixelRatio = 1.0,
    this.safeArea = FakeViewPadding.zero,
    this.textScale = 1.0,
  });

  final Brightness brightness;
  final double devicePixelRatio;
  final String name;
  final FakeViewPadding safeArea;
  final Size size;
  final double textScale;

  Device copyWith({
    Brightness? brightness,
    double? devicePixelRatio,
    String? name,
    FakeViewPadding? safeArea,
    Size? size,
    double? textScale,
  }) =>
      Device(
        brightness: brightness ?? this.brightness,
        devicePixelRatio: devicePixelRatio ?? this.devicePixelRatio,
        name: name ?? this.name,
        safeArea: safeArea ?? this.safeArea,
        size: size ?? this.size,
        textScale: textScale ?? this.textScale,
      );

  Device dark() => copyWith(
        brightness: Brightness.dark,
        name: '${name}_dark',
      );
}
