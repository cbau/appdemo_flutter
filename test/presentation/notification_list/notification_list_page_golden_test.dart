import 'package:appdemo_flutter/data/common/local_service.dart';
import 'package:appdemo_flutter/data/notification/notification_remote_data_source.dart';
import 'package:appdemo_flutter/l10n/gen/app_localizations.g.dart';
import 'package:appdemo_flutter/presentation/navigation/navigation_page.dart';
import 'package:appdemo_flutter/presentation/notification_list/notification_list_page.dart';
import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:fake_cloud_firestore/fake_cloud_firestore.dart';
import 'package:flutter_test/flutter_test.dart';

import '../test_app.dart';
import '../test_devices.dart';

void main() {
  setUpAll(
    () async {
      LocalService().openDatabaseFromMemory = true;
    },
  );

  for (final locale in AppLocalizations.supportedLocales) {
    testWidgets(
      'When notification list page is shown, '
      'and request is not successful '
      'verify it matches the golden tests.',
      (tester) async {
        final fakeFirestore = FakeFirebaseFirestore(
          securityRules: '''
service cloud.firestore {
  match /databases/{database}/documents {
    match /{document=**} {
      allow read: if false;
      allow write: if true;
    }
  }
}
''',
        );
        await fakeFirestore.collection('notifications').add({});
        NotificationRemoteDataSource().firestore = fakeFirestore;

        await tester.pumpWidget(
          TestApp(
            home: NavigationPage(
              child: NotificationListPage(
                isTestMode: true,
              ),
            ),
            locale: locale,
          ),
        );

        await TestDevices().matchesGoldenFiles(
          tester,
          'notification_list_${locale.languageCode}_error',
        );
      },
    );

    testWidgets(
      'When notification list page is shown, '
      'and request is successful '
      'verify it matches the golden tests.',
      (tester) async {
        final fakeFirestore = FakeFirebaseFirestore();
        await fakeFirestore.collection('notifications').add({
          'title': 'Notification Test',
          'message': 'This is a notification test.',
          'timestamp': Timestamp.fromDate(DateTime(2001)),
        });
        NotificationRemoteDataSource().firestore = fakeFirestore;

        await tester.pumpWidget(
          TestApp(
            home: NavigationPage(
              child: NotificationListPage(
                isTestMode: true,
              ),
            ),
            locale: locale,
          ),
        );

        await TestDevices().matchesGoldenFiles(
          tester,
          'notification_list_${locale.languageCode}',
        );
      },
    );
  }
}
