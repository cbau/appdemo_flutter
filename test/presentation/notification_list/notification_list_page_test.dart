import 'package:appdemo_flutter/data/common/local_service.dart';
import 'package:appdemo_flutter/data/notification/notification_remote_data_source.dart';
import 'package:appdemo_flutter/presentation/common/error_dialog.dart';
import 'package:appdemo_flutter/presentation/navigation/navigation_page.dart';
import 'package:appdemo_flutter/presentation/notification_list/notification_list_item.dart';
import 'package:appdemo_flutter/presentation/notification_list/notification_list_page.dart';
import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:fake_cloud_firestore/fake_cloud_firestore.dart';
import 'package:flutter_test/flutter_test.dart';

import '../../data/common/test_data.dart';
import '../test_app.dart';

void main() {
  setUpAll(
    () async {
      LocalService().openDatabaseFromMemory = true;
    },
  );

  testWidgets(
    'When notification list page is shown, '
    'and request is not successful '
    'then display error message.',
    (tester) async {
      final fakeFirestore = FakeFirebaseFirestore(
        securityRules: '''
service cloud.firestore {
  match /databases/{database}/documents {
    match /{document=**} {
      allow read: if false;
      allow write: if true;
    }
  }
}
''',
      );
      await fakeFirestore.collection('notifications').add({});
      NotificationRemoteDataSource().firestore = fakeFirestore;

      await tester.pumpWidget(
        TestApp(
          home: NavigationPage(
            child: NotificationListPage(
              isTestMode: true,
            ),
          ),
        ),
      );

      // Wait until the bloc data has been loaded.
      await tester.pumpAndSettle();

      final dialogFinder = find.byType(ErrorDialog);

      expect(dialogFinder, findsOne);
    },
  );

  testWidgets(
    'When notification list page is shown, '
    'and request is successful '
    'then display as expected.',
    (tester) async {
      final fakeFirestore = FakeFirebaseFirestore();
      await fakeFirestore.collection('notifications').add({
        'title': 'Notification Test',
        'message': 'This is a notification test.',
        'timestamp': Timestamp.fromDate(DateTime(2001)),
      });
      NotificationRemoteDataSource().firestore = fakeFirestore;

      final notification = TestData().notification;

      await tester.pumpWidget(
        TestApp(
          home: NavigationPage(
            child: NotificationListPage(
              isTestMode: true,
            ),
          ),
        ),
      );

      // Wait until the bloc data has been loaded.
      await tester.pumpAndSettle();

      final itemFinder = find.byType(NotificationListItem);
      final titleFinder = find.text(notification.title);

      expect(itemFinder, findsAny);
      expect(titleFinder, findsOne);
    },
  );
}
