import 'package:appdemo_flutter/presentation/common/error_dialog.dart';
import 'package:appdemo_flutter/presentation/navigation/navigation_page.dart';
import 'package:appdemo_flutter/presentation/profile/profile_page.dart';
import 'package:appdemo_flutter/presentation/sign_in/sign_in_page.dart';
import 'package:flutter/widgets.dart';
import 'package:flutter_test/flutter_test.dart';

import '../test_app.dart';

void main() {
  testWidgets(
    'When profile page is shown, '
    'and user is not signed in '
    'then sign in is available.',
    (tester) async {
      await tester.pumpWidget(
        TestApp(
          home: NavigationPage(
            selectedIndex: 3,
            child: ProfilePage(),
          ),
        ),
      );

      final openSignInButtonFinder =
          find.byKey(ProfilePage.openSignInButtonKey);
      final signInPageFinder = find.byType(SignInPage);

      expect(openSignInButtonFinder, findsOne);
      expect(signInPageFinder, findsNothing);

      await tester.tap(openSignInButtonFinder);
      await tester.pumpAndSettle();

      expect(signInPageFinder, findsOne);

      final signInButtonFinder = find.byKey(SignInPage.signInButtonKey);
      expect(signInButtonFinder, findsOne);

      await tester.tap(signInButtonFinder);
      await tester.pumpAndSettle();

      final dialogFinder = find.byType(ErrorDialog);

      expect(dialogFinder, findsNothing, reason: 'Button must be disabled');

      final bloc = tester.widget<SignInPage>(signInPageFinder).bloc;
      bloc.emailManager.controller.text = 'jsmith@example.com';
      bloc.passwordManager.controller.text = 'Abc123';
      bloc.onPasswordChanged('');
      await tester.pumpAndSettle();

      await tester.tap(signInButtonFinder);
      await tester.pumpAndSettle();

      expect(dialogFinder, findsOne, reason: 'Button must be enabled');
    },
  );

  testWidgets(
    'When profile page is shown, '
    'and user is not signed in '
    'then sign up is available.',
    (tester) async {
      await tester.pumpWidget(
        TestApp(
          home: NavigationPage(
            selectedIndex: 3,
            child: ProfilePage(),
          ),
        ),
      );

      final openSignInButtonFinder =
          find.byKey(ProfilePage.openSignInButtonKey);
      final signInPageFinder = find.byType(SignInPage);

      expect(openSignInButtonFinder, findsOne);
      expect(signInPageFinder, findsNothing);

      await tester.tap(openSignInButtonFinder);
      await tester.pumpAndSettle();

      // Change selection to sign up.
      final bloc = tester.widget<SignInPage>(signInPageFinder).bloc;
      bloc.signModeSegmentButtonKey.currentState?.widget.onSelectionChanged
          ?.call({true});
      await tester.pumpAndSettle();

      expect(signInPageFinder, findsOne);

      final scrollableFinder = find.byType(Scrollable).first;
      expect(scrollableFinder, findsOne);

      final signInButtonFinder = find.byKey(SignInPage.signInButtonKey);
      await tester.scrollUntilVisible(
        signInButtonFinder,
        50,
        scrollable: scrollableFinder,
      );
      await tester.pumpAndSettle();

      expect(signInButtonFinder, findsOne);

      await tester.tap(signInButtonFinder);
      await tester.pumpAndSettle();

      final dialogFinder = find.byType(ErrorDialog);

      expect(dialogFinder, findsNothing, reason: 'Button must be disabled');

      bloc.nameManager.controller.text = 'John Smith';
      bloc.onNameChanged('');
      bloc.emailManager.controller.text = 'jsmith@example.com';
      bloc.onEmailChanged('');
      bloc.passwordManager.controller.text = 'Abc123';
      bloc.onPasswordChanged('');
      bloc.passwordConfirmManager.controller.text = 'Abc123';
      bloc.onPasswordConfirmChanged('');
      await tester.pumpAndSettle();

      await tester.tap(signInButtonFinder);
      await tester.pumpAndSettle();

      expect(dialogFinder, findsOne, reason: 'Button must be enabled');
    },
  );
}
