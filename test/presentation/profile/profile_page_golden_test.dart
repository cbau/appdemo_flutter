import 'package:appdemo_flutter/l10n/gen/app_localizations.g.dart';
import 'package:appdemo_flutter/presentation/navigation/navigation_page.dart';
import 'package:appdemo_flutter/presentation/profile/profile_page.dart';
import 'package:appdemo_flutter/presentation/sign_in/sign_in_page.dart';
import 'package:flutter/widgets.dart';
import 'package:flutter_test/flutter_test.dart';

import '../test_app.dart';
import '../test_devices.dart';

void main() {
  for (final locale in AppLocalizations.supportedLocales) {
    testWidgets(
      'When profile page is shown, '
      'verify it matches the golden tests.',
      (tester) async {
        // Pump screen.
        await tester.pumpWidget(
          TestApp(
            home: NavigationPage(
              selectedIndex: 3,
              child: ProfilePage(),
            ),
            locale: locale,
          ),
        );

        // Generate screen goldens.
        await TestDevices().matchesGoldenFiles(
          tester,
          'profile_${locale.languageCode}',
        );
      },
    );

    testWidgets(
      'When profile page is shown, '
      'and sign in button is pressed, '
      'verify it matches the golden tests.',
      (tester) async {
        // Pump screen.
        await tester.pumpWidget(
          TestApp(
            home: NavigationPage(
              selectedIndex: 3,
              child: ProfilePage(),
            ),
            locale: locale,
          ),
        );

        // Open sign in form.
        final openSignInButtonFinder =
            find.byKey(ProfilePage.openSignInButtonKey);
        await tester.tap(openSignInButtonFinder);
        await tester.pumpAndSettle();

        // Generate screen goldens.
        await TestDevices().matchesGoldenFiles(
          tester,
          'profile_${locale.languageCode}_sign_in',
        );
      },
    );

    testWidgets(
      'When profile page is shown, '
      'and sign in is attempted, '
      'verify it matches the golden tests.',
      (tester) async {
        // Pump screen.
        await tester.pumpWidget(
          TestApp(
            home: NavigationPage(
              selectedIndex: 3,
              child: ProfilePage(),
            ),
            locale: locale,
          ),
        );

        // Open sign in form.
        final openSignInButtonFinder =
            find.byKey(ProfilePage.openSignInButtonKey);
        await tester.tap(openSignInButtonFinder);
        await tester.pumpAndSettle();

        // Fill required sign in fields.
        final signInPageFinder = find.byType(SignInPage);
        final bloc = tester.widget<SignInPage>(signInPageFinder).bloc;
        bloc.emailManager.controller.text = 'jsmith@example.com';
        bloc.passwordManager.controller.text = 'Abc123';
        bloc.onPasswordChanged('');
        await tester.pumpAndSettle();

        // Press sign in.
        final signInButtonFinder = find.byKey(SignInPage.signInButtonKey);
        await tester.tap(signInButtonFinder);
        await tester.pumpAndSettle();

        // Generate screen goldens.
        await TestDevices().matchesGoldenFiles(
          tester,
          'profile_${locale.languageCode}_sign_in_error',
        );
      },
    );

    testWidgets(
      'When profile page is shown, '
      'and sign up button is pressed, '
      'verify it matches the golden tests.',
      (tester) async {
        // Pump screen.
        await tester.pumpWidget(
          TestApp(
            home: NavigationPage(
              selectedIndex: 3,
              child: ProfilePage(),
            ),
            locale: locale,
          ),
        );

        // Open sign in form.
        final openSignInButtonFinder =
            find.byKey(ProfilePage.openSignInButtonKey);
        await tester.tap(openSignInButtonFinder);
        await tester.pumpAndSettle();

        // Change to sign up mode.
        final signInPageFinder = find.byType(SignInPage);
        final bloc = tester.widget<SignInPage>(signInPageFinder).bloc;
        bloc.signModeSegmentButtonKey.currentState?.widget.onSelectionChanged
            ?.call({true});
        await tester.pumpAndSettle();

        // Scroll to sign up button.
        final scrollableFinder = find.byType(Scrollable).first;
        final signInButtonFinder = find.byKey(SignInPage.signInButtonKey);
        await tester.scrollUntilVisible(
          signInButtonFinder,
          50,
          scrollable: scrollableFinder,
        );
        await tester.pumpAndSettle();

        // Generate screen goldens.
        await TestDevices().matchesGoldenFiles(
          tester,
          'profile_${locale.languageCode}_sign_up',
        );
      },
    );

    testWidgets(
      'When profile page is shown, '
      'and sign up is attempted, '
      'verify it matches the golden tests.',
      (tester) async {
        // Pump screen.
        await tester.pumpWidget(
          TestApp(
            home: NavigationPage(
              selectedIndex: 3,
              child: ProfilePage(),
            ),
            locale: locale,
          ),
        );

        // Open sign in form.
        final openSignInButtonFinder =
            find.byKey(ProfilePage.openSignInButtonKey);
        await tester.tap(openSignInButtonFinder);
        await tester.pumpAndSettle();

        // Change to sign up mode.
        final signInPageFinder = find.byType(SignInPage);
        final bloc = tester.widget<SignInPage>(signInPageFinder).bloc;
        bloc.signModeSegmentButtonKey.currentState?.widget.onSelectionChanged
            ?.call({true});
        await tester.pumpAndSettle();

        // Fill required sign up fields.
        bloc.nameManager.controller.text = 'John Smith';
        bloc.emailManager.controller.text = 'jsmith@example.com';
        bloc.passwordManager.controller.text = 'Abc123';
        bloc.passwordConfirmManager.controller.text = 'Abc123';
        bloc.onPasswordConfirmChanged('');
        await tester.pumpAndSettle();

        // Scroll to sign up button.
        final scrollableFinder = find.byType(Scrollable).first;
        final signInButtonFinder = find.byKey(SignInPage.signInButtonKey);
        await tester.scrollUntilVisible(
          signInButtonFinder,
          50,
          scrollable: scrollableFinder,
        );
        await tester.pumpAndSettle();

        // Press sign up.
        await tester.tap(signInButtonFinder);
        await tester.pumpAndSettle();

        // Generate screen goldens.
        await TestDevices().matchesGoldenFiles(
          tester,
          'profile_${locale.languageCode}_sign_up_error',
        );
      },
    );
  }
}
