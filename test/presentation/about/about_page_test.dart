import 'package:appdemo_flutter/presentation/about/about_page.dart';
import 'package:appdemo_flutter/presentation/common/bullet_list_item.dart';
import 'package:appdemo_flutter/presentation/navigation/navigation_page.dart';
import 'package:flutter_test/flutter_test.dart';

import '../test_app.dart';

void main() {
  testWidgets(
    'When about page is shown, '
    'then display as expected.',
    (tester) async {
      await tester.pumpWidget(
        const TestApp(
          home: NavigationPage(
            selectedIndex: 2,
            child: AboutPage(),
          ),
        ),
      );

      final bulletListItemFinder = find.byType(BulletListItem);

      expect(bulletListItemFinder, findsAny);
    },
  );
}
