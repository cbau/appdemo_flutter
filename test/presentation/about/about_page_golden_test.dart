import 'package:appdemo_flutter/l10n/gen/app_localizations.g.dart';
import 'package:appdemo_flutter/presentation/about/about_page.dart';
import 'package:appdemo_flutter/presentation/navigation/navigation_page.dart';
import 'package:flutter_test/flutter_test.dart';

import '../test_app.dart';
import '../test_devices.dart';

void main() {
  for (final locale in AppLocalizations.supportedLocales) {
    testWidgets(
      'When about page is shown, '
      'verify it matches the golden tests.',
      (tester) async {
        await tester.pumpWidget(
          TestApp(
            home: const NavigationPage(
              selectedIndex: 2,
              child: AboutPage(),
            ),
            locale: locale,
          ),
        );

        await TestDevices().matchesGoldenFiles(
          tester,
          'about_${locale.languageCode}',
        );
      },
    );
  }
}
