import 'package:appdemo_flutter/l10n/gen/app_localizations.g.dart';
import 'package:appdemo_flutter/presentation/app_theme.dart';
import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';

class TestApp extends StatelessWidget {
  const TestApp({
    this.home,
    this.locale,
    super.key,
  });

  final Widget? home;
  final Locale? locale;

  @override
  Widget build(BuildContext context) => MaterialApp(
        darkTheme: AppTheme().dark,
        home: home,
        locale: locale,
        localizationsDelegates: AppLocalizations.localizationsDelegates,
        supportedLocales: AppLocalizations.supportedLocales,
        theme: AppTheme().light,
      );

  @override
  void debugFillProperties(DiagnosticPropertiesBuilder properties) {
    super.debugFillProperties(properties);
    properties.add(DiagnosticsProperty<Locale?>('locale', locale));
  }
}
