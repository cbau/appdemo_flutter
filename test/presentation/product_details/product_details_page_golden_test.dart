import 'package:appdemo_flutter/data/common/fake_store_service.dart';
import 'package:appdemo_flutter/data/common/local_service.dart';
import 'package:appdemo_flutter/l10n/gen/app_localizations.g.dart';
import 'package:appdemo_flutter/presentation/common/app_network_image.dart';
import 'package:appdemo_flutter/presentation/product_details/product_details_page.dart';
import 'package:flutter/widgets.dart';
import 'package:flutter_test/flutter_test.dart';

import '../../data/common/test_client.dart';
import '../../data/common/test_data.dart';
import '../test_app.dart';
import '../test_devices.dart';

void main() {
  setUpAll(
    () async {
      AppNetworkImage.isTestMode = true;
      LocalService().openDatabaseFromMemory = true;
    },
  );

  for (final locale in AppLocalizations.supportedLocales) {
    testWidgets(
      'When product details page is shown, '
      'and the product is non-existent '
      'verify it matches the golden tests.',
      (tester) async {
        FakeStoreService().setClient(TestClient());

        await tester.pumpWidget(
          TestApp(
            home: ProductDetailsPage(
              productId: 0,
            ),
            locale: locale,
          ),
        );

        await TestDevices().matchesGoldenFiles(
          tester,
          'product_details_${locale.languageCode}_error',
        );
      },
    );

    testWidgets(
      'When product details page is shown, '
      'verify it matches the golden tests.',
      (tester) async {
        FakeStoreService().setClient(TestClient());

        final product = TestData().product;

        await tester.runAsync(
          () async => tester.pumpWidget(
            TestApp(
              home: ProductDetailsPage(
                productId: product.id,
                product: product,
              ),
              locale: locale,
            ),
          ),
        );

        await TestDevices().matchesGoldenFiles(
          tester,
          'product_details_${locale.languageCode}',
        );
      },
    );

    testWidgets(
      'When product details page is shown, '
      'and the screen is scrolled down '
      'verify it matches the golden tests.',
      (tester) async {
        FakeStoreService().setClient(TestClient());

        final product = TestData().product;

        await tester.runAsync(
          () async => tester.pumpWidget(
            TestApp(
              home: ProductDetailsPage(
                productId: product.id,
                product: product,
              ),
              locale: locale,
            ),
          ),
        );

        await TestDevices().matchesGoldenFiles(
          tester,
          'product_details_${locale.languageCode}_scrolled',
          customPump: (tester) async {
            final scrollableFinder = find.byType(Scrollable).first;
            await tester.drag(scrollableFinder, const Offset(0, -300));
            await tester.pumpAndSettle();
          },
        );
      },
    );
  }
}
