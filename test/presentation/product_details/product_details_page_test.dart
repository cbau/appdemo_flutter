import 'package:appdemo_flutter/data/common/fake_store_service.dart';
import 'package:appdemo_flutter/data/common/local_service.dart';
import 'package:appdemo_flutter/presentation/common/app_network_image.dart';
import 'package:appdemo_flutter/presentation/not_found/not_found_page.dart';
import 'package:appdemo_flutter/presentation/product_details/product_details_page.dart';
import 'package:flutter/widgets.dart';
import 'package:flutter_test/flutter_test.dart';

import '../../data/common/test_client.dart';
import '../../data/common/test_data.dart';
import '../test_app.dart';

void main() {
  setUpAll(
    () {
      AppNetworkImage.isTestMode = true;
      LocalService().openDatabaseFromMemory = true;
      FakeStoreService().setClient(TestClient());
    },
  );

  testWidgets(
    'When product details page is shown, '
    'and product is non-existent '
    'then display not found page.',
    (tester) async {
      await tester.pumpWidget(
        TestApp(
          home: ProductDetailsPage(
            productId: 0,
          ),
        ),
      );

      // Wait until the bloc data has been loaded.
      await tester.pumpAndSettle();

      final notFoundPageFinder = find.byType(NotFoundPage);

      expect(notFoundPageFinder, findsOne);
    },
  );

  testWidgets(
    'When product details page is shown, '
    'and product is not received '
    'then display as expected.',
    (tester) async {
      final product = TestData().product;

      await tester.pumpWidget(
        TestApp(
          home: ProductDetailsPage(
            productId: product.id,
          ),
        ),
      );

      // Wait until the bloc data has been loaded.
      await tester.pumpAndSettle();

      final titleFinder = find.text(product.title);
      final descriptionFinder = find.text(product.description);

      expect(titleFinder, findsExactly(2));
      expect(descriptionFinder, findsOne);
    },
  );

  testWidgets(
    'When product details page is shown, '
    'and product is received '
    'then display as expected.',
    (tester) async {
      // This two lines test ProductDetailsContainerSmall as well:
      tester.view.physicalSize = const Size(360, 480);
      tester.view.devicePixelRatio = 1;
      addTearDown(tester.view.reset);

      final product = TestData().product;

      await tester.pumpWidget(
        TestApp(
          home: ProductDetailsPage(
            productId: product.id,
            product: product,
          ),
        ),
      );

      final titleFinder = find.text(product.title);
      final descriptionFinder = find.text(product.description);

      final scrollableFinder = find.byType(Scrollable).last;
      await tester.scrollUntilVisible(
        descriptionFinder,
        50,
        scrollable: scrollableFinder,
      );
      await tester.pumpAndSettle();

      expect(titleFinder, findsExactly(2));
      expect(descriptionFinder, findsOne);
    },
  );
}
