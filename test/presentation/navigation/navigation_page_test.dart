import 'package:appdemo_flutter/presentation/navigation/navigation_page.dart';
import 'package:flutter/material.dart';
import 'package:flutter_test/flutter_test.dart';

import '../test_app.dart';

void main() {
  testWidgets(
    'When navigation page is shown, '
    'and screen is small, '
    'then display as expected.',
    (tester) async {
      tester.view.physicalSize = const Size(360, 480);
      tester.view.devicePixelRatio = 1;
      addTearDown(tester.view.reset);

      await tester.pumpWidget(
        const TestApp(
          home: NavigationPage(),
        ),
      );

      final navigationBarFinder = find.byType(NavigationBar);
      final navigationRailFinder = find.byType(NavigationRail);

      expect(navigationBarFinder, findsOne);
      expect(navigationRailFinder, findsNothing);
    },
  );

  testWidgets(
    'When navigation page is shown, '
    'and screen is big, '
    'then display as expected.',
    (tester) async {
      tester.view.physicalSize = const Size(1024, 768);
      tester.view.devicePixelRatio = 1;
      addTearDown(tester.view.reset);

      await tester.pumpWidget(
        const TestApp(
          home: NavigationPage(),
        ),
      );

      final navigationBarFinder = find.byType(BottomNavigationBar);
      final navigationRailFinder = find.byType(NavigationRail);

      expect(navigationBarFinder, findsNothing);
      expect(navigationRailFinder, findsOne);
    },
  );
}
