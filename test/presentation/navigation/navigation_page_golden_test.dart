import 'package:appdemo_flutter/l10n/gen/app_localizations.g.dart';
import 'package:appdemo_flutter/presentation/navigation/navigation_page.dart';
import 'package:flutter_test/flutter_test.dart';

import '../test_app.dart';
import '../test_devices.dart';

void main() {
  for (final locale in AppLocalizations.supportedLocales) {
    testWidgets(
      'When navigation list page is shown, '
      'verify it matches the golden tests.',
      (tester) async {
        await tester.pumpWidget(
          TestApp(
            home: const NavigationPage(),
            locale: locale,
          ),
        );

        await TestDevices().matchesGoldenFiles(
          tester,
          'navigation_${locale.languageCode}',
        );
      },
    );
  }
}
