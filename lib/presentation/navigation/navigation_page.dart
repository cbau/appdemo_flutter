import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';

import '../../l10n/gen/app_localizations.g.dart';
import 'navigation_scaffold.dart';

/// The navigation page.
class NavigationPage extends StatefulWidget {
  /// Initialices the [child] and [key] on this [NavigationPage].
  const NavigationPage({
    this.child,
    this.onDestinationSelected,
    this.selectedIndex = 0,
    super.key,
  });

  /// The child of this navigation page.
  final Widget? child;

  /// The selected index in the navigation.
  final int selectedIndex;

  /// The action when a destination is selected in the navigation.
  final void Function(int index)? onDestinationSelected;

  @override
  State<NavigationPage> createState() => _NavigationPageState();

  // coverage:ignore-start
  @override
  void debugFillProperties(DiagnosticPropertiesBuilder properties) {
    super.debugFillProperties(properties);
    properties
      ..add(IntProperty('selectedIndex', selectedIndex))
      ..add(
        ObjectFlagProperty<void Function(int index)?>.has(
          'onDestinationSelected',
          onDestinationSelected,
        ),
      );
  }
  // coverage:ignore-end
}

class _NavigationPageState extends State<NavigationPage> {
  late final _items = [
    NavigationDestination(
      icon: const Icon(Icons.notifications_outlined),
      label: _strings.notifications,
      selectedIcon: const Icon(Icons.notifications),
    ),
    NavigationDestination(
      icon: const Icon(Icons.store_outlined),
      label: _strings.products,
      selectedIcon: const Icon(Icons.store),
    ),
    NavigationDestination(
      icon: const Icon(Icons.info_outlined),
      label: _strings.about,
      selectedIcon: const Icon(Icons.info),
    ),
    NavigationDestination(
      icon: const Icon(Icons.person_outlined),
      label: _strings.profile,
      selectedIcon: const Icon(Icons.person),
    ),
  ];

  late final _strings = AppLocalizations.of(context)!;

  @override
  Widget build(BuildContext context) => NavigationScaffold(
        body: widget.child,
        selectedIndex: widget.selectedIndex,
        onDestinationSelected: widget.onDestinationSelected,
        items: _items,
      );
}
