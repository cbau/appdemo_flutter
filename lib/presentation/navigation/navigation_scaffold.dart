import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';

/// A scaffold that includes a responsive navigation area.
class NavigationScaffold extends StatelessWidget {
  /// Initializes the properties on this [NavigationScaffold].
  const NavigationScaffold({
    required this.items,
    this.appBar,
    this.body,
    this.onDestinationSelected,
    this.selectedIndex = 0,
    super.key,
  });

  /// An app bar to display at the top of the scaffold.
  final PreferredSizeWidget? appBar;

  /// The primary content of the scaffold.
  final Widget? body;

  /// The navigation items.
  final List<NavigationDestination> items;

  /// The selected index in the navigation.
  final int selectedIndex;

  /// The action when a destination is selected in the navigation.
  final ValueChanged<int>? onDestinationSelected;

  @override
  Widget build(BuildContext context) => LayoutBuilder(
        builder: (context, constrains) => Scaffold(
          appBar: appBar,
          body: constrains.maxWidth < 600
              ? body
              : Row(
                  children: [
                    NavigationRail(
                      destinations: [
                        ...items.map(
                          (e) => NavigationRailDestination(
                            icon: e.icon,
                            selectedIcon: e.selectedIcon,
                            label: Text(e.label),
                          ),
                        ),
                      ],
                      extended: constrains.maxWidth >= 900,
                      labelType: constrains.maxWidth >= 900
                          ? null
                          : NavigationRailLabelType.all,
                      onDestinationSelected: onDestinationSelected,
                      selectedIndex: selectedIndex,
                    ),
                    Expanded(
                      child: body ?? const SizedBox(),
                    ),
                  ],
                ),
          bottomNavigationBar: constrains.maxWidth < 600
              ? NavigationBar(
                  destinations: items,
                  onDestinationSelected: onDestinationSelected,
                  selectedIndex: selectedIndex,
                )
              : null,
        ),
      );

  // coverage:ignore-start
  @override
  void debugFillProperties(DiagnosticPropertiesBuilder properties) {
    super.debugFillProperties(properties);
    properties
      ..add(IterableProperty<NavigationDestination>('items', items))
      ..add(IntProperty('selectedIndex', selectedIndex))
      ..add(
        ObjectFlagProperty<ValueChanged<int>?>.has(
          'onDestinationSelected',
          onDestinationSelected,
        ),
      );
  }
  // coverage:ignore-end
}
