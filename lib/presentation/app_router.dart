import 'package:flutter/material.dart';
import 'package:go_router/go_router.dart';

import '../data/product/product.dart';
import 'about/about_page.dart';
import 'navigation/navigation_page.dart';
import 'not_found/not_found_page.dart';
import 'notification_list/notification_list_page.dart';
import 'product_details/product_details_page.dart';
import 'product_list/product_list_page.dart';
import 'profile/profile_page.dart';

/// The application router.
class AppRouter {
  /// Factory instance constructor.
  factory AppRouter() => _instance;

  /// Private constructor for this class.
  AppRouter._();

  static final AppRouter _instance = AppRouter._();

  final _rootNavigatorKey = GlobalKey<NavigatorState>();

  /// The main router for this application.
  late final router = GoRouter(
    errorBuilder: (context, state) => const NotFoundPage(),
    initialLocation: '/notifications',
    navigatorKey: _rootNavigatorKey,
    routes: [
      GoRoute(
        path: '/',
        redirect: (context, state) => '/notifications',
      ),
      // Remove base URL on deep links:
      GoRoute(
        path: '/appdemo_flutter/:path',
        redirect: (context, state) => '/${state.pathParameters['path']}',
        routes: [
          GoRoute(
            path: ':id',
            redirect: (context, state) =>
                '/${state.pathParameters['path']}/${state.pathParameters['id']}',
          ),
        ],
      ),
      /*
      GoRoute(
        path: '/appdemo_flutter/:path/:id',
        redirect: (context, state) =>
            '/${state.params['path']}/${state.params['id']}',
      ),
      */
      StatefulShellRoute.indexedStack(
        builder: (context, state, navigationShell) => NavigationPage(
          onDestinationSelected: navigationShell.goBranch,
          selectedIndex: navigationShell.currentIndex,
          child: navigationShell,
        ),
        branches: [
          StatefulShellBranch(
            routes: [
              GoRoute(
                name: NotificationListPage.routeName,
                pageBuilder: (context, state) => _FadeTransitionPage<void>(
                  key: state.pageKey,
                  child: NotificationListPage(),
                ),
                path: '/notifications',
              ),
            ],
          ),
          StatefulShellBranch(
            routes: [
              GoRoute(
                name: ProductListPage.routeName,
                pageBuilder: (context, state) => _FadeTransitionPage<void>(
                  key: state.pageKey,
                  child: ProductListPage(),
                ),
                path: '/products',
                routes: [
                  GoRoute(
                    builder: (context, state) => ProductDetailsPage(
                      productId:
                          int.tryParse(state.pathParameters['id'] ?? '0') ?? 0,
                      product: state.extra as Product?,
                    ),
                    name: ProductDetailsPage.routeName,
                    parentNavigatorKey: _rootNavigatorKey,
                    path: ':id',
                  ),
                ],
              ),
            ],
          ),
          StatefulShellBranch(
            routes: [
              GoRoute(
                name: AboutPage.routeName,
                pageBuilder: (context, state) => _FadeTransitionPage<void>(
                  key: state.pageKey,
                  child: const AboutPage(),
                ),
                path: '/about',
              ),
            ],
          ),
          StatefulShellBranch(
            routes: [
              GoRoute(
                name: ProfilePage.routeName,
                pageBuilder: (context, state) => _FadeTransitionPage<void>(
                  key: state.pageKey,
                  child: ProfilePage(),
                ),
                path: '/profile',
              ),
            ],
          ),
        ],
      ),
    ],
  );
}

class _FadeTransitionPage<T> extends CustomTransitionPage<T> {
  _FadeTransitionPage({required super.child, super.key})
      : super(
          transitionsBuilder: (context, animation, secondaryAnimation, child) =>
              FadeTransition(
            opacity: animation,
            child: child,
          ),
        );
}
