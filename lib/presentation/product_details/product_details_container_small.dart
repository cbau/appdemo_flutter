import 'package:flutter/material.dart';
import 'package:flutter/rendering.dart';
import 'package:share_plus/share_plus.dart';

import '../../data/product/product.dart';
import '../../l10n/gen/app_localizations.g.dart';
import '../common/app_network_image.dart';
import 'product_details_content.dart';

/// The product details container on small screens.
class ProductDetailsContainerSmall extends StatefulWidget {
  /// Initializes the [item] and [key] on this [ProductDetailsContainerSmall].
  const ProductDetailsContainerSmall({
    required this.item,
    super.key,
  });

  /// The item to display.
  final Product item;

  @override
  State<ProductDetailsContainerSmall> createState() =>
      _ProductDetailsContainerSmallState();

  // coverage:ignore-start
  @override
  void debugFillProperties(DiagnosticPropertiesBuilder properties) {
    super.debugFillProperties(properties);
    properties.add(DiagnosticsProperty<Product>('item', item));
  }
  // coverage:ignore-end
}

class _ProductDetailsContainerSmallState
    extends State<ProductDetailsContainerSmall>
    with TickerProviderStateMixin<ProductDetailsContainerSmall> {
  late final _fabAnimation = AnimationController(
    duration: kThemeAnimationDuration,
    value: 1,
    vsync: this,
  );

  late final ScrollController _scrollController = ScrollController(
    initialScrollOffset: 120,
  )..addListener(() {
      if (_scrollController.position.userScrollDirection !=
              ScrollDirection.idle &&
          _scrollController.position.maxScrollExtent !=
              _scrollController.position.minScrollExtent) {
        _scrollController.position.userScrollDirection ==
                ScrollDirection.forward
            ? _fabAnimation.forward()
            : _fabAnimation.reverse();
      }
    });

  @override
  Widget build(BuildContext context) {
    final strings = AppLocalizations.of(context)!;

    return Scaffold(
      body: NestedScrollView(
        controller: _scrollController,
        headerSliverBuilder: (context, innerBoxIsScrolled) => [
          SliverOverlapAbsorber(
            handle: NestedScrollView.sliverOverlapAbsorberHandleFor(context),
            sliver: SliverAppBar(
              expandedHeight: widget.item.image.isEmpty ? null : 360,
              flexibleSpace: FlexibleSpaceBar(
                background: widget.item.image.isEmpty
                    ? null
                    : Hero(
                        tag: 'thumbnail${widget.item.id}',
                        child: AppNetworkImage(
                          color: Theme.of(context).primaryColor,
                          colorBlendMode: BlendMode.multiply,
                          imageUrl: widget.item.image,
                          width: MediaQuery.sizeOf(context).width,
                        ),
                      ),
                title: Text(
                  widget.item.title,
                  maxLines: 1,
                  overflow: TextOverflow.ellipsis,
                ),
              ),
              forceElevated: innerBoxIsScrolled,
              pinned: true,
            ),
          ),
        ],
        body: Builder(
          builder: (context) => ProductDetailsContent(
            item: widget.item,
            useOverlapInjector: true,
          ),
        ),
      ),
      floatingActionButton: ScaleTransition(
        scale: _fabAnimation,
        child: FloatingActionButton(
          onPressed: () async => Share.share(widget.item.url),
          tooltip: strings.actionShare,
          child: const Icon(Icons.share),
        ),
      ),
    );
  }

  @override
  void dispose() {
    _fabAnimation.dispose();
    _scrollController.dispose();
    super.dispose();
  }
}
