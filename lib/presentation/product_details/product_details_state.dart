import '../../data/product/product.dart';

/// The product details page state base.
sealed class ProductDetailsState {
  /// Initializes properties on this [ProductDetailsState].
  const ProductDetailsState();
}

/// The product details page state error.
class ProductDetailsStateError extends ProductDetailsState {
  /// Initializes properties on this [ProductDetailsStateError].
  const ProductDetailsStateError({
    this.errorMessage,
  });

  /// The error message.
  final String? errorMessage;
}

/// The product details page state loading.
class ProductDetailsStateLoading extends ProductDetailsState {
  /// Initializes properties on this [ProductDetailsStateLoading].
  const ProductDetailsStateLoading();
}

/// The product details page state success.
class ProductDetailsStateSuccess extends ProductDetailsState {
  /// Initializes properties on this [ProductDetailsStateSuccess].
  const ProductDetailsStateSuccess({
    this.product,
  });

  /// The product to display.
  final Product? product;
}
