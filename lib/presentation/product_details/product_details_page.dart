import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';

import '../../data/product/product.dart';
import '../not_found/not_found_page.dart';
import 'product_details_container.dart';
import 'product_details_container_small.dart';
import 'product_details_cubit.dart';
import 'product_details_state.dart';

/// The page with the details of a product.
class ProductDetailsPage extends StatelessWidget {
  /// Initializes the [product] and [key] on this [ProductDetailsPage].
  ProductDetailsPage({
    required int productId,
    Product? product,
    super.key,
  }) : _bloc = ProductDetailsCubit(
          productId: productId,
          product: product,
        );

  /// The route name for this page.
  static const String routeName = 'ProductDetails';

  /// The business logic component for this page.
  final ProductDetailsCubit _bloc;

  @override
  Widget build(BuildContext context) =>
      BlocBuilder<ProductDetailsCubit, ProductDetailsState>(
        bloc: _bloc,
        builder: (context, state) => switch (state) {
          ProductDetailsStateLoading() => const Center(
              child: CircularProgressIndicator(),
            ),
          ProductDetailsStateError() => const NotFoundPage(),
          ProductDetailsStateSuccess() => LayoutBuilder(
              builder: (context, constraints) => constraints.minWidth < 600
                  ? ProductDetailsContainerSmall(item: state.product!)
                  : ProductDetailsContainer(item: state.product!),
            ),
        },
      );
}
