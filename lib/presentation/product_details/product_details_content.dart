import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';

import '../../data/product/product.dart';

/// The product details content.
class ProductDetailsContent extends StatelessWidget {
  /// Initializes the properties on this [ProductDetailsContent].
  const ProductDetailsContent({
    required this.item,
    this.useOverlapInjector = false,
    super.key,
  });

  /// The item to display.
  final Product item;

  /// Whether this widget should use overlap injector.
  final bool useOverlapInjector;

  @override
  Widget build(BuildContext context) {
    final textTheme = Theme.of(context).textTheme;
    return CustomScrollView(
      slivers: [
        if (useOverlapInjector)
          SliverOverlapInjector(
            handle: NestedScrollView.sliverOverlapAbsorberHandleFor(context),
          ),
        SliverPadding(
          padding: const EdgeInsets.all(16),
          sliver: SliverList(
            delegate: SliverChildListDelegate(
              [
                Text(
                  item.title,
                  style: textTheme.headlineMedium,
                ),
                const SizedBox(
                  height: 8,
                ),
                Row(
                  children: [
                    Text(
                      '\$${item.price.toStringAsFixed(2)}',
                      style: textTheme.titleMedium,
                    ),
                    const Spacer(),
                    const Icon(Icons.star),
                    Text(
                      item.rating.rate.toStringAsFixed(1),
                      style: textTheme.bodyMedium,
                    ),
                  ],
                ),
                const SizedBox(
                  height: 8,
                ),
                Text(
                  item.category,
                  style: textTheme.labelMedium?.copyWith(color: Colors.grey),
                ),
                const SizedBox(
                  height: 8,
                ),
                Text(
                  item.description,
                  style: textTheme.bodyMedium,
                ),
                /*
                RichText(
                  text: HTML.toTextSpan(
                    context,
                    item.description,
                    defaultTextStyle:
                        textTheme.bodyMedium,
                  ),
                ),
                */
              ],
            ),
          ),
        ),
      ],
    );
  }

  // coverage:ignore-start
  @override
  void debugFillProperties(DiagnosticPropertiesBuilder properties) {
    super.debugFillProperties(properties);
    properties
      ..add(DiagnosticsProperty<Product>('item', item))
      ..add(
        DiagnosticsProperty<bool>(
          'useOverlapInjector',
          useOverlapInjector,
        ),
      );
  }
  // coverage:ignore-end
}
