import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';
import 'package:share_plus/share_plus.dart';

import '../../data/product/product.dart';
import '../../l10n/gen/app_localizations.g.dart';
import '../common/app_network_image.dart';
import 'product_details_content.dart';

/// The product details container.
class ProductDetailsContainer extends StatelessWidget {
  /// Initializes the [item] and [key] on this [ProductDetailsContainer].
  const ProductDetailsContainer({
    required this.item,
    super.key,
  });

  /// The item to display.
  final Product item;

  @override
  Widget build(BuildContext context) {
    final strings = AppLocalizations.of(context)!;

    return Scaffold(
      appBar: AppBar(
        title: Text(
          item.title,
          maxLines: 1,
          overflow: TextOverflow.ellipsis,
        ),
      ),
      body: Row(
        children: [
          Padding(
            padding: const EdgeInsets.all(16),
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                IconButton(
                  icon: const Icon(Icons.share),
                  onPressed: () async => Share.share(item.url),
                  tooltip: strings.actionShare,
                ),
                SizedBox(
                  width: 240,
                  child: item.image.isEmpty
                      ? null
                      : Hero(
                          tag: 'thumbnail${item.id}',
                          child: AppNetworkImage(
                            imageUrl: item.image,
                            width: MediaQuery.sizeOf(context).width,
                          ),
                        ),
                ),
              ],
            ),
          ),
          Expanded(
            child: ProductDetailsContent(item: item),
          ),
        ],
      ),
    );
  }

  // coverage:ignore-start
  @override
  void debugFillProperties(DiagnosticPropertiesBuilder properties) {
    super.debugFillProperties(properties);
    properties.add(DiagnosticsProperty<Product>('item', item));
  }
  // coverage:ignore-end
}
