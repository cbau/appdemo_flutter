import 'dart:async';

import 'package:flutter/foundation.dart';
import 'package:flutter_bloc/flutter_bloc.dart';

import '../../data/product/product.dart';
import '../../data/product/product_repository.dart';
import 'product_details_state.dart';

/// The product details page logic.
class ProductDetailsCubit extends Cubit<ProductDetailsState> {
  /// Initializes the [productId] and [product] on this [ProductDetailsCubit].
  ProductDetailsCubit({
    required int productId,
    Product? product,
  }) : super(
          product == null
              ? const ProductDetailsStateLoading()
              : ProductDetailsStateSuccess(
                  product: product,
                ),
        ) {
    if (product == null) {
      unawaited(_getProduct(id: productId));
    }
  }

  Future<void> _getProduct({required int id}) async {
    emit(
      const ProductDetailsStateLoading(),
    );
    try {
      emit(
        ProductDetailsStateSuccess(
          product: await ProductRepository().getById(id),
        ),
      );
    } on Exception catch (e, s) {
      debugPrint('$e\n$s');
      emit(
        ProductDetailsStateError(
          errorMessage: e.toString(),
        ),
      );
    }
  }
}
