import 'package:flutter/material.dart';
import 'package:flutter/scheduler.dart';
import 'package:flutter_bloc/flutter_bloc.dart';

import '../../data/product/product_repository.dart';
import 'product_list_state.dart';

/// The product list page logic.
class ProductListCubit extends Cubit<ProductListState> {
  /// Creates a [ProductListCubit].
  ProductListCubit() : super(const ProductListStateLoading()) {
    _init();
  }

  /// The refresh indicator key.
  final refreshIndicatorKey = GlobalKey<RefreshIndicatorState>();

  /// Refreshes the data.
  Future<void> refresh() async {
    await _fetch();
  }

  Future<void> _fetch() async {
    //emit(state.copyWith(errorMessage: ''));
    ProductRepository().getAll().listen(
      (event) {
        debugPrint('Item count: ${event.length}');
        emit(
          ProductListStateSuccess(
            items: event,
          ),
        );
      },
      onError: (Object? error) => emit(
        ProductListStateError(
          errorMessage: error.toString(),
        ),
      ),
    );
  }

  void _init() {
    // Retrieves the remote data for the first time while showing the refresh
    // indicator after the first build.
    SchedulerBinding.instance.addPostFrameCallback((timeStamp) async {
      await refreshIndicatorKey.currentState?.show();
    });
  }
}
