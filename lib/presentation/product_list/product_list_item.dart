import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';
import 'package:go_router/go_router.dart';

import '../../data/product/product.dart';
import '../common/app_network_image.dart';
import '../product_details/product_details_page.dart';

/// The product list item.
class ProductListItem extends StatelessWidget {
  /// Initializes the [item] and [key] on this [ProductListItem].
  const ProductListItem({
    required this.item,
    super.key,
  });

  /// The item to display.
  final Product item;

  @override
  Widget build(BuildContext context) {
    final textTheme = Theme.of(context).primaryTextTheme;

    return Card(
      color: Theme.of(context).primaryColor,
      child: InkWell(
        onTap: () async => GoRouter.of(context).goNamed(
          ProductDetailsPage.routeName,
          pathParameters: {
            'id': item.id.toString(),
          },
          extra: item,
        ),
        child: Column(
          children: [
            Expanded(
              child: ColoredBox(
                color: Colors.white,
                child: item.image.isEmpty
                    ? const SizedBox()
                    : Hero(
                        tag: 'thumbnail${item.id}',
                        child: AppNetworkImage(
                          errorWidget: (context, url, error) =>
                              const Icon(Icons.broken_image),
                          imageUrl: item.image,
                          progressIndicatorBuilder: (context, url, progress) =>
                              Center(
                            child: CircularProgressIndicator(
                              value: progress,
                            ),
                          ),
                          width: MediaQuery.sizeOf(context).width,
                        ),
                      ),
              ),
            ),
            Padding(
              padding: const EdgeInsets.all(8),
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  Row(
                    children: [
                      Expanded(
                        child: Text(
                          '\$${item.price.toStringAsFixed(2)}',
                          style: textTheme.titleMedium,
                        ),
                      ),
                      const Icon(
                        Icons.star,
                        color: Colors.white,
                      ),
                      Text(
                        item.rating.rate.toStringAsFixed(1),
                        style: textTheme.bodyMedium,
                      ),
                    ],
                  ),
                  const SizedBox(
                    height: 8,
                  ),
                  Text(
                    item.title,
                    maxLines: 1,
                    style: textTheme.bodyMedium,
                  ),
                ],
              ),
            ),
          ],
        ),
      ),
    );
  }

  // coverage:ignore-start
  @override
  void debugFillProperties(DiagnosticPropertiesBuilder properties) {
    super.debugFillProperties(properties);
    properties.add(DiagnosticsProperty<Product>('item', item));
  }
  // coverage:ignore-end
}
