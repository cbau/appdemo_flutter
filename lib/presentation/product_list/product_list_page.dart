import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';

import '../../data/product/product.dart';
import '../../l10n/gen/app_localizations.g.dart';
import '../common/error_dialog.dart';
import 'product_list_cubit.dart';
import 'product_list_item.dart';
import 'product_list_state.dart';

/// The page with the list of products.
class ProductListPage extends StatelessWidget {
  /// Initializes the [key] on this [ProductListPage].
  ProductListPage({super.key}) : _bloc = ProductListCubit();

  /// The route name for this page.
  static const String routeName = 'ProductList';

  final ProductListCubit _bloc;

  @override
  Widget build(BuildContext context) {
    final strings = AppLocalizations.of(context)!;

    return Scaffold(
      appBar: AppBar(
        title: Text(strings.products),
      ),
      body: BlocConsumer<ProductListCubit, ProductListState>(
        bloc: _bloc,
        builder: (context, state) {
          final items =
              state is ProductListStateSuccess ? state.items : <Product>[];
          return Stack(
            children: [
              RefreshIndicator(
                key: _bloc.refreshIndicatorKey,
                onRefresh: _bloc.refresh,
                child: GridView.builder(
                  gridDelegate: const SliverGridDelegateWithMaxCrossAxisExtent(
                    childAspectRatio: 0.8,
                    crossAxisSpacing: 8,
                    mainAxisSpacing: 8,
                    maxCrossAxisExtent: 240,
                  ),
                  itemCount: items.length,
                  itemBuilder: (context, index) =>
                      ProductListItem(item: items[index]),
                  padding: const EdgeInsets.all(8),
                ),
              ),
              if (items.isEmpty)
                Center(
                  child: Text(strings.productsEmptyError),
                ),
            ],
          );
        },
        listener: (context, state) async {
          if (state is ProductListStateError) {
            await showDialog<void>(
              context: context,
              builder: (context) => ErrorDialog(state.errorMessage),
            );
          }
        },
        buildWhen: (previous, current) => current is! ProductListStateError,
        listenWhen: (previous, current) => current is ProductListStateError,
      ),
    );
  }
}
