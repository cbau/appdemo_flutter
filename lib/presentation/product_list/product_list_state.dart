import '../../data/product/product.dart';

/// The product list page state base.
sealed class ProductListState {
  /// Initializes properties on this [ProductListState].
  const ProductListState();
}

/// The product list page state error.
class ProductListStateError extends ProductListState {
  /// Initializes properties on this [ProductListStateError].
  const ProductListStateError({
    this.errorMessage = '',
  });

  /// The error message to display.
  final String errorMessage;
}

/// The product list page state loading.
class ProductListStateLoading extends ProductListState {
  /// Initializes properties on this [ProductListStateLoading].
  const ProductListStateLoading();
}

/// The product list page state success.
class ProductListStateSuccess extends ProductListState {
  /// Initializes properties on this [ProductListStateSuccess].
  const ProductListStateSuccess({
    this.items = const [],
  });

  /// The retrieved items.
  final List<Product> items;
}
