/// The profile page state base.
sealed class ProfileState {
  const ProfileState();
}

/// The profile page state loading.
class ProfileStateLoading extends ProfileState {
  /// Initializes properties on this [ProfileStateLoading].
  const ProfileStateLoading();
}

/// The profile page state success.
class ProfileStateSuccess extends ProfileState {
  /// Initializes properties on this [ProfileStateSuccess].
  const ProfileStateSuccess({
    this.version = '',
  });

  /// The application current version.
  final String version;
}
