import 'dart:async';

import 'package:flutter/services.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:package_info_plus/package_info_plus.dart';

import '../../build_config.dart';
import 'profile_state.dart';

/// The profile page logic.
class ProfileCubit extends Cubit<ProfileState> {
  /// Creates a [ProfileCubit].
  ProfileCubit() : super(const ProfileStateLoading()) {
    unawaited(_init());
  }

  Future<void> _init() async {
    const env = BuildConfig.flavor == 'prod' ? '' : ' (${BuildConfig.flavor})';
    try {
      final packageInfo = await PackageInfo.fromPlatform();

      emit(
        ProfileStateSuccess(
          version: 'v${packageInfo.version}+${packageInfo.buildNumber}$env',
        ),
      );
    } on MissingPluginException {
      emit(
        const ProfileStateSuccess(
          version: 'v0.0.0+0$env',
        ),
      );
    }
  }
}
