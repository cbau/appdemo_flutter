import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';

import '../../l10n/gen/app_localizations.g.dart';
import '../sign_in/sign_in_page.dart';
import 'profile_cubit.dart';
import 'profile_state.dart';

/// The page with the user profile information.
class ProfilePage extends StatelessWidget {
  /// Initializes the [key] on this [ProfilePage].
  ProfilePage({super.key}) : _bloc = ProfileCubit();

  /// The route name for this page.
  static const String routeName = 'Profile';

  /// The key used for the open sign in button.
  static const ValueKey<String> openSignInButtonKey =
      ValueKey('OpenSignInButton');

  final ProfileCubit _bloc;

  @override
  Widget build(BuildContext context) {
    final strings = AppLocalizations.of(context)!;

    return Scaffold(
      appBar: AppBar(
        title: Text(strings.profile),
      ),
      body: BlocBuilder<ProfileCubit, ProfileState>(
        bloc: _bloc,
        builder: (context, state) => ListView(
          padding: const EdgeInsets.all(16),
          children: [
            switch (state) {
              ProfileStateLoading() => const SizedBox.shrink(),
              ProfileStateSuccess() => Text(
                  state.version,
                  style: Theme.of(context).textTheme.bodySmall,
                  textAlign: TextAlign.end,
                ),
            },
            Text(strings.signInBenefits),
            const SizedBox(
              height: 16,
            ),
            Center(
              child: ElevatedButton(
                key: openSignInButtonKey,
                onPressed: () async => SignInPage().showAsModalBottomSheet(
                  context: context,
                ),
                child: Text(strings.actionSignIn),
              ),
            ),
          ],
        ),
      ),
    );
  }
}
