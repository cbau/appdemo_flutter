import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';

import '../../data/notification/notification.dart' as notification;
import '../../l10n/gen/app_localizations.g.dart';
import '../common/error_dialog.dart';
import 'notification_list_cubit.dart';
import 'notification_list_item.dart';
import 'notification_list_state.dart';

/// The page with the list of notifications.
class NotificationListPage extends StatelessWidget {
  /// Initializes the [key] on this [NotificationListPage].
  NotificationListPage({
    bool isTestMode = false,
    super.key,
  }) : _bloc = NotificationListCubit(isTestMode: isTestMode);

  /// The route name for this page.
  static const String routeName = 'NotificationList';

  final NotificationListCubit _bloc;

  @override
  Widget build(BuildContext context) {
    final strings = AppLocalizations.of(context)!;

    return Scaffold(
      appBar: AppBar(
        title: Text(strings.notifications),
      ),
      body: BlocConsumer<NotificationListCubit, NotificationListState>(
        bloc: _bloc,
        builder: (context, state) {
          final items = state is NotificationListStateSuccess
              ? state.items
              : <notification.Notification>[];
          return Stack(
            children: [
              RefreshIndicator(
                key: _bloc.refreshIndicatorKey,
                onRefresh: _bloc.refresh,
                child: ListView.builder(
                  itemCount: items.length,
                  itemBuilder: (context, index) =>
                      NotificationListItem(item: items[index]),
                  padding: const EdgeInsets.all(8),
                ),
              ),
              if (items.isEmpty)
                Center(
                  child: Text(strings.notificationsEmptyError),
                ),
            ],
          );
        },
        listener: (context, state) async {
          if (state is NotificationListStateError) {
            await showDialog<void>(
              context: context,
              builder: (context) => ErrorDialog(state.errorMessage),
            );
          }
        },
        buildWhen: (previous, current) =>
            current is! NotificationListStateError,
        listenWhen: (previous, current) =>
            current is NotificationListStateError,
      ),
    );
  }
}
