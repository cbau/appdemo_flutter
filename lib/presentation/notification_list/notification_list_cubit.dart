import 'dart:async';

import 'package:firebase_messaging/firebase_messaging.dart';
import 'package:flutter/material.dart';
import 'package:flutter/scheduler.dart';
import 'package:flutter_bloc/flutter_bloc.dart';

import '../../data/notification/notification_repository.dart';
import 'notification_list_state.dart';

/// The notification list page logic.
class NotificationListCubit extends Cubit<NotificationListState> {
  /// Creates a [NotificationListCubit].
  NotificationListCubit({
    bool isTestMode = false,
  })  : _isTestMode = isTestMode,
        super(const NotificationListStateLoading()) {
    _init();
  }

  final bool _isTestMode;

  /// The refresh indicator key.
  final refreshIndicatorKey = GlobalKey<RefreshIndicatorState>();

  /// Refreshes the data.
  Future<void> refresh() async {
    await _fetch();
  }

  Future<void> _fetch() async {
    //emit(state.copyWith(errorMessage: ''));
    NotificationRepository().getAll().listen(
          (event) => emit(
            NotificationListStateSuccess(
              items: event..sort((x, y) => y.timestamp.compareTo(x.timestamp)),
            ),
          ),
          onError: (Object? error) => emit(
            NotificationListStateError(
              errorMessage: error.toString(),
            ),
          ),
        );
  }

  void _init() {
    // Retrieves the remote data for the first time while showing the refresh
    // indicator after the first build.
    SchedulerBinding.instance.addPostFrameCallback((timeStamp) async {
      await refreshIndicatorKey.currentState?.show();
    });

    unawaited(_requestNotificationPermission());
  }

  Future<void> _requestNotificationPermission() async {
    if (_isTestMode) {
      return;
    }

    // coverage:ignore-start
    final settings = await FirebaseMessaging.instance.requestPermission();

    switch (settings.authorizationStatus) {
      case AuthorizationStatus.authorized:
        debugPrint('Authorization status: User granted permission');
      case AuthorizationStatus.provisional:
        debugPrint('Authorization status: User granted provisional permission');
      case AuthorizationStatus.denied:
      case AuthorizationStatus.notDetermined:
        debugPrint(
          'Authorization status: User declined or has not accepted permission',
        );
    }
    // coverage:ignore-end
  }
}
