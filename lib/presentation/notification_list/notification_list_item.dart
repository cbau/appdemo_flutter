import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';
import 'package:intl/intl.dart';

import '../../data/notification/notification.dart' as models;

/// The notification list item.
class NotificationListItem extends StatelessWidget {
  /// Initializes the [item] and [key] on this [NotificationListItem].
  const NotificationListItem({
    required this.item,
    super.key,
  });

  /// The item to display.
  final models.Notification item;

  @override
  Widget build(BuildContext context) => Card(
        child: Padding(
          padding: const EdgeInsets.all(8),
          child: Column(
            children: [
              Row(
                children: [
                  Expanded(
                    child: Text(
                      item.title,
                      style: Theme.of(context).textTheme.titleLarge,
                    ),
                  ),
                  Text(
                    '${DateFormat.yMMMEd().format(item.timestamp)} '
                    '${DateFormat.Hm().format(item.timestamp)}',
                    style: Theme.of(context).textTheme.bodySmall,
                  ),
                ],
              ),
              const SizedBox(
                height: 8,
              ),
              Text(item.message),
            ],
          ),
        ),
      );

  // coverage:ignore-start
  @override
  void debugFillProperties(DiagnosticPropertiesBuilder properties) {
    super.debugFillProperties(properties);
    properties.add(DiagnosticsProperty<models.Notification>('item', item));
  }
  // coverage:ignore-end
}
