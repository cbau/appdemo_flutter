import '../../data/notification/notification.dart';

/// The notification list page state base.
sealed class NotificationListState {
  /// Initializes properties on this [NotificationListState].
  const NotificationListState();
}

/// The notification list page state error.
class NotificationListStateError extends NotificationListState {
  /// Initializes properties on this [NotificationListStateError].
  const NotificationListStateError({
    this.errorMessage = '',
  });

  /// The error message to display.
  final String errorMessage;
}

/// The notification list page state loading.
class NotificationListStateLoading extends NotificationListState {
  /// Initializes properties on this [NotificationListStateLoading].
  const NotificationListStateLoading();
}

/// The notification list page state success.
class NotificationListStateSuccess extends NotificationListState {
  /// Initializes properties on this [NotificationListStateSuccess].
  const NotificationListStateSuccess({
    this.items = const [],
  });

  /// The retrieved items.
  final List<Notification> items;
}
