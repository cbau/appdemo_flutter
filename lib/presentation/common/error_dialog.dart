import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';

import '../../l10n/gen/app_localizations.g.dart';

/// A widget to display an error inside a dialog.
class ErrorDialog extends StatelessWidget {
  /// Initializes the [error] and [key] for this [ErrorDialog].
  const ErrorDialog(
    this.error, {
    super.key,
  });

  /// The error that will be displayed inside this dialog.
  final String error;

  @override
  Widget build(BuildContext context) {
    final strings = AppLocalizations.of(context)!;

    return AlertDialog(
      title: Text(strings.error),
      content: Text(error),
      actions: [
        TextButton(
          onPressed: () => Navigator.of(context).pop(),
          child: Text(strings.actionOk),
        ),
      ],
    );
  }

  // coverage:ignore-start
  @override
  void debugFillProperties(DiagnosticPropertiesBuilder properties) {
    super.debugFillProperties(properties);
    properties.add(StringProperty('error', error));
  }
  // coverage:ignore-end
}
