import 'package:flutter/foundation.dart';
import 'package:flutter/widgets.dart';

/// A bullet list item.
class BulletListItem extends StatelessWidget {
  /// Initializes the [data], [bullet] and [key] for this [BulletListItem].
  const BulletListItem(
    this.data, {
    this.bullet = const Text('•'),
    super.key,
  });

  /// The widget that will be used as a bullet.
  final Widget bullet;

  /// The data for this bullet list item.
  final String data;

  @override
  Widget build(BuildContext context) => Row(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          bullet,
          const SizedBox(
            width: 8,
          ),
          Expanded(
            child: Text(data),
          ),
        ],
      );

  // coverage:ignore-start
  @override
  void debugFillProperties(DiagnosticPropertiesBuilder properties) {
    super.debugFillProperties(properties);
    properties.add(StringProperty('data', data));
  }
  // coverage:ignore-end
}
