import 'package:cached_network_image/cached_network_image.dart';
import 'package:flutter/foundation.dart';
import 'package:flutter/widgets.dart';

/// The network image widget used by this application.
class AppNetworkImage extends StatelessWidget {
  /// Initializes the parameters for this [AppNetworkImage].
  const AppNetworkImage({
    required this.imageUrl,
    this.color,
    this.colorBlendMode,
    this.errorWidget,
    this.height,
    this.progressIndicatorBuilder,
    this.width,
    super.key,
  });

  /// Whether this app's network images are in testing mode.
  @visibleForTesting
  static bool isTestMode = false;

  /// If non-null, this color is blended with each image pixel using
  /// [colorBlendMode].
  final Color? color;

  /// Used to combine [color] with this image.
  ///
  /// The default is [BlendMode.srcIn]. In terms of the blend mode, [color] is
  /// the source and this image is the destination.
  ///
  /// See also:
  ///
  ///  * [BlendMode], which includes an illustration of the effect of each
  /// blend mode.
  final BlendMode? colorBlendMode;

  /// Widget displayed while the target [imageUrl] failed loading.
  final Widget Function(BuildContext context, String url, Object error)?
      errorWidget;

  /// The target image that is displayed.
  final String imageUrl;

  /// If non-null, require the image to have this height.
  ///
  /// If null, the image will pick a size that best preserves its intrinsic
  /// aspect ratio. This may result in a sudden change if the size of the
  /// placeholder widget does not match that of the target image. The size is
  /// also affected by the scale factor.
  final double? height;

  /// Widget displayed while the target [imageUrl] is loading.
  final Widget Function(BuildContext context, String url, double? progress)?
      progressIndicatorBuilder;

  /// If non-null, require the image to have this width.
  ///
  /// If null, the image will pick a size that best preserves its intrinsic
  /// aspect ratio. This may result in a sudden change if the size of the
  /// placeholder widget does not match that of the target image. The size is
  /// also affected by the scale factor.
  final double? width;

  // coverage:ignore-start
  @override
  Widget build(BuildContext context) => isTestMode
      ? const Placeholder()
      : CachedNetworkImage(
          color: color,
          colorBlendMode: colorBlendMode,
          errorWidget: errorWidget,
          imageUrl: imageUrl,
          height: height,
          progressIndicatorBuilder: progressIndicatorBuilder == null
              ? null
              : (context, url, progress) => progressIndicatorBuilder!
                  .call(context, url, progress.progress),
          width: width,
        );

  @override
  void debugFillProperties(DiagnosticPropertiesBuilder properties) {
    super.debugFillProperties(properties);
    properties
      ..add(ColorProperty('color', color))
      ..add(EnumProperty<BlendMode?>('colorBlendMode', colorBlendMode))
      ..add(
        ObjectFlagProperty<
            Widget Function(BuildContext p1, String p2, Object p3)?>.has(
          'errorWidget',
          errorWidget,
        ),
      )
      ..add(StringProperty('imageUrl', imageUrl))
      ..add(DoubleProperty('height', height))
      ..add(
        ObjectFlagProperty<
            Widget Function(BuildContext p1, String p2, double? p3)?>.has(
          'progressIndicatorBuilder',
          progressIndicatorBuilder,
        ),
      )
      ..add(DoubleProperty('width', width));
  }
  // coverage:ignore-end
}
