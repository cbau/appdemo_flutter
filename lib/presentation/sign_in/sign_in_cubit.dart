import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:intl/intl.dart';

import '../../data/user_session/user_session.dart';
import '../../data/user_session/user_session_repository.dart';
import 'sign_in_state.dart';

/// The sign in page logic.
class SignInCubit extends Cubit<SignInState> {
  /// Initializes the [dateFormat] on this [SignInCubit].
  SignInCubit()
      : super(
          const SignInStateLoading(
            isSigningUp: false,
          ),
        ) {
    _init();
  }

  /// The birthday controller.
  final birthdayController = TextEditingController();

  /// The date format.
  String dateFormat = 'MMM dd, yyyy';

  /// The email text field manager.
  final emailManager = _TextFieldManager();

  /// The name text field manager.
  final nameManager = _TextFieldManager();

  /// The password text field manager.
  final passwordManager = _TextFieldManager();

  /// The password confirm text field manager.
  final passwordConfirmManager = _TextFieldManager();

  /// The sign mode segmented button key.
  final signModeSegmentButtonKey = GlobalKey<State<SegmentedButton<bool>>>();

  var _birthday = DateTime.now();
  final _countries = const [
    'Autralia',
    'Canada',
    'United Kingdom',
    'United States',
    'Other',
  ];
  var _currentCountry = 'United States';
  var _isPasswordObscured = true;
  var _isPasswordConfirmObscured = true;
  var _isSigningUp = false;
  var _maxBirthday = DateTime.now();
  final _minBirthday = DateTime(1900);

  bool get _isFormValid =>
      (emailManager.key.currentState?.isValid ?? false) &&
      (passwordManager.key.currentState?.isValid ?? false) &&
      (!_isSigningUp ||
          ((nameManager.key.currentState?.isValid ?? false) &&
              (passwordConfirmManager.key.currentState?.isValid ?? false)));

  void _emitDefaultState() {
    emit(
      SignInStateDefault(
        birthday: _birthday,
        countries: _countries,
        currentCountry: _currentCountry,
        isPasswordConfirmObscured: _isPasswordConfirmObscured,
        isPasswordObscured: _isPasswordObscured,
        isFormValid: _isFormValid,
        isSigningUp: _isSigningUp,
        maxBirthday: _maxBirthday,
        minBirthday: _minBirthday,
      ),
    );
  }

  void _init() {
    final now = DateTime.now();
    _birthday = DateTime(now.year - 13, now.month, now.day);
    _maxBirthday = _birthday;
    birthdayController.text = DateFormat(dateFormat).format(_birthday);

    _emitDefaultState();
  }

  /// Action called when the country changes.
  void onCountryChanged(String? value) {
    _currentCountry = value ?? '';
    _emitDefaultState();
  }

  /// Action called when the email changes.
  void onEmailChanged(String? value) {
    emailManager.key.currentState?.validate();
    _emitDefaultState();
  }

  /// Action called when the name changes.
  void onNameChanged(String? value) {
    nameManager.key.currentState?.validate();
    _emitDefaultState();
  }

  /// Action called when the password changes.
  void onPasswordChanged(String? value) {
    passwordManager.key.currentState?.validate();
    _emitDefaultState();
  }

  /// Action called when the password confirm changes.
  void onPasswordConfirmChanged(String? value) {
    passwordConfirmManager.key.currentState?.validate();
    _emitDefaultState();
  }

  /// Action called when the sign mode selection is changed.
  void onSignModeSelectionChanged(Set<bool> newSelection) {
    _isSigningUp = newSelection.first;
    _emitDefaultState();
  }

  /// Sets the birthday.
  void setBirthday(DateTime? date) {
    if (date != null) {
      _birthday = date;
      birthdayController.text = DateFormat(dateFormat).format(_birthday);
      _emitDefaultState();
    }
  }

  /// Signs up the user.
  Future<void> signUp() async {
    emit(
      SignInStateLoading(
        isSigningUp: _isSigningUp,
      ),
    );
    try {
      _isSigningUp
          ? await UserSessionRepository().signUp(
              user: UserSession(
                birthday: _birthday,
                country: _currentCountry,
                email: emailManager.controller.value.text,
                name: nameManager.controller.value.text,
              ),
              password: passwordManager.controller.value.text,
            )
          : await UserSessionRepository().signIn(
              username: emailManager.controller.value.text,
              password: passwordManager.controller.value.text,
            );
      emit(const SignInStateSuccess());
    } on Exception catch (e) {
      emit(
        SignInStateError(
          errorMessage: e.toString(),
        ),
      );
      _emitDefaultState();
    }
  }

  /// Toggles the password obscured value.
  void togglePasswordObcured() {
    _isPasswordObscured = !_isPasswordObscured;
    _emitDefaultState();
  }

  /// Toggles the password confirm obscured value.
  void togglePasswordConfirmObcured() {
    _isPasswordConfirmObscured = !_isPasswordConfirmObscured;
    _emitDefaultState();
  }
}

class _TextFieldManager {
  final controller = TextEditingController();
  final key = GlobalKey<FormFieldState<String>>();
}
