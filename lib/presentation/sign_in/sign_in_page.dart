import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';

import '../../extensions.dart';
import '../../l10n/gen/app_localizations.g.dart';
import '../common/error_dialog.dart';
import 'sign_in_cubit.dart';
import 'sign_in_state.dart';

/// The page that allows an user to sign in.
class SignInPage extends StatelessWidget {
  /// Initializes the [key] on this [SignInPage].
  SignInPage({
    super.key,
  }) : bloc = SignInCubit();

  /// The key used for the sign in button.
  static const ValueKey<String> signInButtonKey = ValueKey('SignInButton');

  /// The bloc for this widget.
  @visibleForTesting
  final SignInCubit bloc;

  @override
  Widget build(BuildContext context) {
    final strings = AppLocalizations.of(context)!;
    bloc.dateFormat = strings.dateFormat;

    return BlocConsumer<SignInCubit, SignInState>(
      bloc: bloc,
      builder: (context, state) => Form(
        child: ListView(
          padding: const EdgeInsets.all(16),
          children: [
            Align(
              alignment: Alignment.topLeft,
              child: IconButton(
                icon: const Icon(Icons.close),
                onPressed: () => Navigator.of(context).pop(),
              ),
            ),
            Center(
              child: SegmentedButton(
                key: bloc.signModeSegmentButtonKey,
                selected: {
                  state.isSigningUp,
                },
                onSelectionChanged: bloc.onSignModeSelectionChanged,
                segments: [
                  ButtonSegment(
                    value: false,
                    icon: const Icon(Icons.person_search),
                    label: Text(strings.signIn),
                  ),
                  ButtonSegment(
                    value: true,
                    icon: const Icon(Icons.person_add),
                    label: Text(strings.signUp),
                  ),
                ],
              ),
            ),
            const SizedBox(
              height: 16,
            ),
            if (state.isSigningUp)
              TextFormField(
                controller: bloc.nameManager.controller,
                decoration: InputDecoration(
                  border: const OutlineInputBorder(),
                  hintText: strings.fullNameHint,
                  labelText: strings.fullName,
                  prefixIcon: const Icon(Icons.person),
                ),
                key: bloc.nameManager.key,
                keyboardType: TextInputType.name,
                onChanged: bloc.onNameChanged,
                textCapitalization: TextCapitalization.words,
                textInputAction: TextInputAction.next,
                validator: (value) =>
                    value?.isNotEmpty ?? true ? null : strings.fieldEmptyError,
              ),
            if (state.isSigningUp)
              const SizedBox(
                height: 16,
              ),
            if (state.isSigningUp)
              DropdownButtonFormField<String>(
                decoration: InputDecoration(
                  border: const OutlineInputBorder(),
                  labelText: strings.country,
                  prefixIcon: const Icon(Icons.flag),
                ),
                items: state is SignInStateDefault
                    ? state.countries
                        .map(
                          (e) => DropdownMenuItem(
                            value: e,
                            child: Text(e),
                          ),
                        )
                        .toList()
                    : [],
                onChanged: bloc.onCountryChanged,
                value:
                    state is SignInStateDefault ? state.currentCountry : null,
              ),
            if (state.isSigningUp)
              const SizedBox(
                height: 16,
              ),
            if (state.isSigningUp)
              TextFormField(
                controller: bloc.birthdayController,
                decoration: InputDecoration(
                  border: const OutlineInputBorder(),
                  hintText: strings.birthdayHint,
                  labelText: strings.birthday,
                  prefixIcon: const Icon(Icons.date_range),
                ),
                readOnly: true,
                onTap: () async {
                  if (state is SignInStateDefault) {
                    bloc.setBirthday(
                      await showDatePicker(
                        context: context,
                        initialDatePickerMode: DatePickerMode.year,
                        initialDate: state.birthday,
                        firstDate: state.minBirthday,
                        lastDate: state.maxBirthday,
                      ),
                    );
                  }
                },
              ),
            if (state.isSigningUp)
              const SizedBox(
                height: 16,
              ),
            TextFormField(
              controller: bloc.emailManager.controller,
              decoration: InputDecoration(
                border: const OutlineInputBorder(),
                hintText: strings.emailHint,
                labelText: strings.email,
                prefixIcon: const Icon(Icons.email),
              ),
              key: bloc.emailManager.key,
              keyboardType: TextInputType.emailAddress,
              onChanged: bloc.onEmailChanged,
              textInputAction: TextInputAction.next,
              validator: (value) => value?.isValidEmail ?? true
                  ? null
                  : strings.emailInvalidError,
            ),
            const SizedBox(
              height: 16,
            ),
            TextFormField(
              controller: bloc.passwordManager.controller,
              decoration: InputDecoration(
                border: const OutlineInputBorder(),
                hintText: strings.passwordHint,
                labelText: strings.password,
                prefixIcon: const Icon(Icons.password),
                suffixIcon: IconButton(
                  onPressed: bloc.togglePasswordObcured,
                  icon: Icon(
                    state is! SignInStateDefault || state.isPasswordObscured
                        ? Icons.visibility_off
                        : Icons.visibility,
                  ),
                ),
              ),
              key: bloc.passwordManager.key,
              keyboardType: TextInputType.visiblePassword,
              obscureText:
                  state is! SignInStateDefault || state.isPasswordObscured,
              onChanged: bloc.onPasswordChanged,
              textInputAction: state.isSigningUp
                  ? TextInputAction.next
                  : TextInputAction.done,
              validator: (value) =>
                  value?.isNotEmpty ?? true ? null : strings.fieldEmptyError,
            ),
            const SizedBox(
              height: 16,
            ),
            if (state.isSigningUp)
              TextFormField(
                controller: bloc.passwordConfirmManager.controller,
                decoration: InputDecoration(
                  border: const OutlineInputBorder(),
                  hintText: strings.passwordHint,
                  labelText: strings.passwordConfirm,
                  prefixIcon: const Icon(Icons.password),
                  suffixIcon: IconButton(
                    onPressed: bloc.togglePasswordConfirmObcured,
                    icon: Icon(
                      state is! SignInStateDefault ||
                              state.isPasswordConfirmObscured
                          ? Icons.visibility_off
                          : Icons.visibility,
                    ),
                  ),
                ),
                key: bloc.passwordConfirmManager.key,
                keyboardType: TextInputType.visiblePassword,
                obscureText: state is! SignInStateDefault ||
                    state.isPasswordConfirmObscured,
                onChanged: bloc.onPasswordConfirmChanged,
                validator: (value) =>
                    value == bloc.passwordManager.key.currentState?.value
                        ? null
                        : strings.passwordMatchError,
              ),
            if (state.isSigningUp)
              const SizedBox(
                height: 16,
              ),
            Center(
              child: ElevatedButton(
                key: signInButtonKey,
                onPressed: state is SignInStateDefault && state.isFormValid
                    ? bloc.signUp
                    : null,
                child: Row(
                  mainAxisSize: MainAxisSize.min,
                  children: [
                    if (state is SignInStateLoading)
                      const CircularProgressIndicator.adaptive(),
                    Text(
                      state.isSigningUp
                          ? strings.actionSignUp
                          : strings.actionSignIn,
                    ),
                  ],
                ),
              ),
            ),
          ],
        ),
      ),
      listener: (context, state) async {
        if (state is SignInStateError) {
          await showDialog<void>(
            context: context,
            builder: (context) => ErrorDialog(state.errorMessage),
          );
        } else if (state is SignInStateSuccess) {
          Navigator.of(context).pop();
        }
      },
      buildWhen: (previous, current) =>
          current is! SignInStateError && current is! SignInStateSuccess,
      listenWhen: (previous, current) =>
          current is SignInStateError || current is SignInStateSuccess,
    );
  }

  // coverage:ignore-start
  @override
  void debugFillProperties(DiagnosticPropertiesBuilder properties) {
    super.debugFillProperties(properties);
    properties.add(DiagnosticsProperty<SignInCubit>('bloc', bloc));
  }
  // coverage:ignore-end

  /// Shows this page as a modal bottom sheet.
  Future<void> showAsModalBottomSheet({
    required BuildContext context,
  }) async =>
      showModalBottomSheet<void>(
        builder: (context) => this,
        context: context,
        constraints: BoxConstraints(
          maxHeight: MediaQuery.sizeOf(context).height - 100,
        ),
        isScrollControlled: true,
        /*
        shape: const RoundedRectangleBorder(
          borderRadius: BorderRadius.only(
            topLeft: Radius.circular(16),
            topRight: Radius.circular(16),
          ),
        ),
        */
      );
}
