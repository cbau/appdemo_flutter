/// The sign in page state base.
sealed class SignInState {
  /// Initializes properties on this [SignInState].
  const SignInState({
    required this.isSigningUp,
  });

  /// Whether the user is signing up.
  final bool isSigningUp;
}

/// The sign in page state default.
class SignInStateDefault extends SignInState {
  /// Initializes properties on this [SignInStateDefault].
  const SignInStateDefault({
    required this.birthday,
    required this.countries,
    required this.currentCountry,
    required this.isFormValid,
    required this.isPasswordObscured,
    required this.isPasswordConfirmObscured,
    required super.isSigningUp,
    required this.maxBirthday,
    required this.minBirthday,
  });

  /// The selected birdthday.
  final DateTime birthday;

  /// The countries available.
  final List<String> countries;

  /// The selected country.
  final String currentCountry;

  /// Whether the form is valid.
  final bool isFormValid;

  /// Whether the password is obscured.
  final bool isPasswordObscured;

  /// Whether the password confirm is obscured.
  final bool isPasswordConfirmObscured;

  /// The maximum birthday date available.
  final DateTime maxBirthday;

  /// The minimun birthday date available.
  final DateTime minBirthday;
}

/// The sign in page state loading.
class SignInStateLoading extends SignInState {
  /// Initializes properties on this [SignInStateLoading].
  const SignInStateLoading({
    required super.isSigningUp,
  });
}

/// The sign in page state error.
class SignInStateError extends SignInState {
  /// Initializes properties on this [SignInStateError].
  const SignInStateError({
    this.errorMessage = '',
    super.isSigningUp = false,
  });

  /// The error message to display.
  final String errorMessage;
}

/// The sign in page state success.
class SignInStateSuccess extends SignInState {
  /// Initializes properties on this [SignInStateSuccess].
  const SignInStateSuccess({
    super.isSigningUp = false,
  });
}
