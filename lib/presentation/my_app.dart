import 'package:flutter/material.dart';

import '../l10n/gen/app_localizations.g.dart';
import 'app_router.dart';
import 'app_theme.dart';

/// The application root.
class MyApp extends StatelessWidget {
  /// Initializes the [key] for this [MyApp].
  const MyApp({super.key});

  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) => MaterialApp.router(
        darkTheme: AppTheme().dark,
        localizationsDelegates: AppLocalizations.localizationsDelegates,
        routerConfig: AppRouter().router,
        supportedLocales: AppLocalizations.supportedLocales,
        theme: AppTheme().light,
        //themeMode: ThemeMode.dark,
        title: 'AppDemo',
      );
}
