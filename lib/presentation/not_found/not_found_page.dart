import 'package:flutter/material.dart';
import 'package:go_router/go_router.dart';

import '../../l10n/gen/app_localizations.g.dart';

/// The page that will be displayed if the current route is invalid.
class NotFoundPage extends StatelessWidget {
  /// Initialices the [key] on this [NotFoundPage].
  const NotFoundPage({super.key});

  @override
  Widget build(BuildContext context) {
    final strings = AppLocalizations.of(context)!;

    return Scaffold(
      appBar: AppBar(
        title: Text(strings.webNotFoundError),
      ),
      body: ListView(
        padding: const EdgeInsets.all(16),
        children: [
          Text(strings.webNotFoundErrorDetails),
          const SizedBox(
            height: 16,
          ),
          ElevatedButton(
            onPressed: () {
              GoRouter.of(context).go('/');
            },
            child: Text(strings.actionReturnHome),
          ),
        ],
      ),
    );
  }
}
