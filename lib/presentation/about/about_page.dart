import 'package:flutter/material.dart';

import '../../l10n/gen/app_localizations.g.dart';
import '../common/bullet_list_item.dart';

/// The page with the information about this application.
class AboutPage extends StatelessWidget {
  /// Initializes the [key] on this [AboutPage].
  const AboutPage({super.key});

  /// The route name for this page.
  static const String routeName = 'About';

  @override
  Widget build(BuildContext context) {
    final strings = AppLocalizations.of(context)!;

    return Scaffold(
      appBar: AppBar(
        title: Text(strings.about),
      ),
      body: ListView(
        padding: const EdgeInsets.all(16),
        children: [
          Text(
            strings.appName,
            style: Theme.of(context).textTheme.titleLarge,
          ),
          const SizedBox(
            height: 16,
          ),
          Text(strings.aboutWelcome),
          const SizedBox(
            height: 16,
          ),
          Text(strings.aboutDescription),
          const SizedBox(
            height: 16,
          ),
          Text(strings.aboutFeatures),
          const SizedBox(
            height: 16,
          ),
          BulletListItem(strings.featureResponsiveness),
          BulletListItem(strings.featureLocalization),
          BulletListItem(strings.featureApi),
          BulletListItem(strings.featureLocalPersistence),
          BulletListItem(strings.featureFieldValidation),
          BulletListItem(strings.featureFirebaseCore),
          BulletListItem(strings.featureFirebaseMessaging),
          BulletListItem(strings.featureFirebaseFirestore),
        ],
      ),
    );
  }
}
