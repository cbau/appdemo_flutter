import 'package:firebase_messaging/firebase_messaging.dart';
import 'package:flutter/material.dart';
import 'package:flutter_local_notifications/flutter_local_notifications.dart';
import 'package:package_info_plus/package_info_plus.dart';

/// The notification manager.
class NotificationManager {
  /// Factory instance constructor.
  factory NotificationManager() => _instance;

  /// Private constructor for this class.
  NotificationManager._() {
    FirebaseMessaging.onMessage.listen((message) async {
      final notification = message.notification;
      final android = message.notification?.android;

      debugPrint('Firebase notification received:'
          '${notification?.title} - ${notification?.body}');
      if (notification != null && android != null) {
        await FlutterLocalNotificationsPlugin().show(
          notification.hashCode,
          notification.title ?? _appName,
          notification.body,
          NotificationDetails(
            android: AndroidNotificationDetails(
              android.channelId ?? 'notifications',
              'Notifications',
              color: Colors.green,
              icon: android.smallIcon ?? 'ic_baseline_notifications_24',
            ),
          ),
        );
      }
    });
  }

  static final NotificationManager _instance = NotificationManager._();

  late String _appName;

  /// Initialized the notification manager.
  Future<void> init() async {
    final packageInfo = await PackageInfo.fromPlatform();
    _appName = packageInfo.appName;
    /*
    await FlutterLocalNotificationsPlugin().initialize(
      const InitializationSettings(
        android: AndroidInitializationSettings('ic_baseline_notifications_24'),
      ),
    );
    */
  }
}
