import 'dart:io';

import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';

/// The application theme.
class AppTheme {
  /// Factory instance constructor.
  factory AppTheme() => _instance;

  /// Private constructor for this class.
  AppTheme._();

  static final AppTheme _instance = AppTheme._();

  ThemeData? _dark;
  ThemeData? _light;

  /// The dark theme data for this application.
  ThemeData get dark => _dark ??= _build(isLight: false);

  /// The light theme data for this application.
  ThemeData get light => _light ??= _build(isLight: true);

  ThemeData _build({required bool isLight}) => ThemeData(
        brightness: isLight ? Brightness.light : Brightness.dark,
        colorSchemeSeed: kIsWeb
            ? Colors.deepPurple
            : Platform.isAndroid
                ? Colors.green
                : Platform.isIOS || Platform.isMacOS
                    ? Colors.blue
                    : Colors.indigo,
      );
}
