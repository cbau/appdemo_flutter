import 'app_localizations.g.dart';

// ignore_for_file: type=lint

/// The translations for Spanish Castilian (`es`).
class AppLocalizationsEs extends AppLocalizations {
  AppLocalizationsEs([String locale = 'es']) : super(locale);

  @override
  String get about => 'Acerca de';

  @override
  String get aboutDescription => 'La aplicación actual funciona como una demostración de algunas de las características más comunes en aplicaciones desarrolladas.';

  @override
  String get aboutFeatures => 'Esta aplicación incluye las siguientes características:';

  @override
  String get aboutWelcome => 'Gracias por descargar esta app demo.';

  @override
  String get actionOk => 'OK';

  @override
  String get actionReturnHome => 'Regresar a inicio';

  @override
  String get actionShare => 'Compartir';

  @override
  String get actionSignIn => 'Entrar';

  @override
  String get actionSignUp => 'Registrar';

  @override
  String get appName => 'AppDemo';

  @override
  String get birthday => 'Fecha de nacimiento';

  @override
  String get birthdayHint => 'DD/MM/YYYY';

  @override
  String get country => 'País';

  @override
  String get dateFormat => 'MMM dd, yyyy';

  @override
  String get email => 'Correo electrónico';

  @override
  String get emailHint => 'jlopez@example.com';

  @override
  String get emailInvalidError => 'Este campo no tiene un formato válido de correo electrónico.';

  @override
  String get error => 'Error';

  @override
  String get featureApi => 'Consumo de Rest API y procesamiento de JSON.';

  @override
  String get featureFieldValidation => 'Validación de campos.';

  @override
  String get featureFirebaseCore => 'Integración de Firebase Analytics y Crashlytics.';

  @override
  String get featureFirebaseFirestore => 'Consumo de datos de Firebase Cloud Firestore.';

  @override
  String get featureFirebaseMessaging => 'Integración de Firebase Cloud Messaging para notificationes push.';

  @override
  String get featureLocalization => 'Localización a multiples lenguajes.';

  @override
  String get featureLocalPersistence => 'Persistencia local.';

  @override
  String get featureResponsiveness => 'Responsividad multiplataforma.';

  @override
  String get fieldEmptyError => 'Este campo no debe estar vacío.';

  @override
  String get fullName => 'Nombre completo';

  @override
  String get fullNameHint => 'Juan López';

  @override
  String get notifications => 'Notificaciones';

  @override
  String get notificationsEmptyError => 'No hay notificaciones disponibles';

  @override
  String get password => 'Contraseña';

  @override
  String get passwordConfirm => 'Confirmar contraseña';

  @override
  String get passwordHint => '******';

  @override
  String get passwordMatchError => 'Las contraseñas no coinciden.';

  @override
  String get products => 'Productos';

  @override
  String get productsEmptyError => 'No hay productos disponibles';

  @override
  String get profile => 'Perfil';

  @override
  String get signIn => 'Entra';

  @override
  String get signInBenefits => '¡Registrate ahora para tomar ventaja de los beneficios de nuestra comunidad!';

  @override
  String get signUp => 'Registrate';

  @override
  String get webNotFoundError => '404 No Encontrado';

  @override
  String get webNotFoundErrorDetails => 'Lo sentimos, no pudimos encontrar la página que buscabas.';
}
