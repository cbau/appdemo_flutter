import 'app_localizations.g.dart';

// ignore_for_file: type=lint

/// The translations for English (`en`).
class AppLocalizationsEn extends AppLocalizations {
  AppLocalizationsEn([String locale = 'en']) : super(locale);

  @override
  String get about => 'About';

  @override
  String get aboutDescription => 'The current app works as a showcase for some of the most common features on developed apps.';

  @override
  String get aboutFeatures => 'This app includes the next features:';

  @override
  String get aboutWelcome => 'Thank you for downloading this app demo.';

  @override
  String get actionOk => 'OK';

  @override
  String get actionReturnHome => 'Return to home';

  @override
  String get actionShare => 'Share';

  @override
  String get actionSignIn => 'Sign in';

  @override
  String get actionSignUp => 'Sign up';

  @override
  String get appName => 'AppDemo';

  @override
  String get birthday => 'Birthday';

  @override
  String get birthdayHint => 'MM/DD/YYYY';

  @override
  String get country => 'Country';

  @override
  String get dateFormat => 'MMM dd, yyyy';

  @override
  String get email => 'E-mail';

  @override
  String get emailHint => 'jsmith@example.com';

  @override
  String get emailInvalidError => 'This field is not in a valid email format.';

  @override
  String get error => 'Error';

  @override
  String get featureApi => 'Rest API consuming & JSON processing.';

  @override
  String get featureFieldValidation => 'Field validation.';

  @override
  String get featureFirebaseCore => 'Firebase Analytics & Crashlytics integration.';

  @override
  String get featureFirebaseFirestore => 'Firebase Cloud Firestore data consuming.';

  @override
  String get featureFirebaseMessaging => 'Firebase Cloud Messaging integration for push notifications.';

  @override
  String get featureLocalization => 'Localization on multiple languages.';

  @override
  String get featureLocalPersistence => 'Local persistence.';

  @override
  String get featureResponsiveness => 'Multiplatform responsiveness.';

  @override
  String get fieldEmptyError => 'This field must not be empty.';

  @override
  String get fullName => 'Full name';

  @override
  String get fullNameHint => 'John Smith';

  @override
  String get notifications => 'Notifications';

  @override
  String get notificationsEmptyError => 'No notifications available';

  @override
  String get password => 'Password';

  @override
  String get passwordConfirm => 'Confirm password';

  @override
  String get passwordHint => '******';

  @override
  String get passwordMatchError => 'The passwords doesn\'t match.';

  @override
  String get products => 'Products';

  @override
  String get productsEmptyError => 'No products available';

  @override
  String get profile => 'Profile';

  @override
  String get signIn => 'Sign in';

  @override
  String get signInBenefits => 'Sign in now to take advantage of our community benefits!';

  @override
  String get signUp => 'Sign up';

  @override
  String get webNotFoundError => '404 Not Found';

  @override
  String get webNotFoundErrorDetails => 'Sorry, we couldn\'t find the page you where looking for.';
}
