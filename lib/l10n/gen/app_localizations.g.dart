import 'dart:async';

import 'package:flutter/foundation.dart';
import 'package:flutter/widgets.dart';
import 'package:flutter_localizations/flutter_localizations.dart';
import 'package:intl/intl.dart' as intl;

import 'app_localizations_en.g.dart';
import 'app_localizations_es.g.dart';
import 'app_localizations_pt.g.dart';

// ignore_for_file: type=lint

/// Callers can lookup localized strings with an instance of AppLocalizations
/// returned by `AppLocalizations.of(context)`.
///
/// Applications need to include `AppLocalizations.delegate()` in their app's
/// `localizationDelegates` list, and the locales they support in the app's
/// `supportedLocales` list. For example:
///
/// ```dart
/// import 'gen/app_localizations.g.dart';
///
/// return MaterialApp(
///   localizationsDelegates: AppLocalizations.localizationsDelegates,
///   supportedLocales: AppLocalizations.supportedLocales,
///   home: MyApplicationHome(),
/// );
/// ```
///
/// ## Update pubspec.yaml
///
/// Please make sure to update your pubspec.yaml to include the following
/// packages:
///
/// ```yaml
/// dependencies:
///   # Internationalization support.
///   flutter_localizations:
///     sdk: flutter
///   intl: any # Use the pinned version from flutter_localizations
///
///   # Rest of dependencies
/// ```
///
/// ## iOS Applications
///
/// iOS applications define key application metadata, including supported
/// locales, in an Info.plist file that is built into the application bundle.
/// To configure the locales supported by your app, you’ll need to edit this
/// file.
///
/// First, open your project’s ios/Runner.xcworkspace Xcode workspace file.
/// Then, in the Project Navigator, open the Info.plist file under the Runner
/// project’s Runner folder.
///
/// Next, select the Information Property List item, select Add Item from the
/// Editor menu, then select Localizations from the pop-up menu.
///
/// Select and expand the newly-created Localizations item then, for each
/// locale your application supports, add a new item and select the locale
/// you wish to add from the pop-up menu in the Value field. This list should
/// be consistent with the languages listed in the AppLocalizations.supportedLocales
/// property.
abstract class AppLocalizations {
  AppLocalizations(String locale) : localeName = intl.Intl.canonicalizedLocale(locale.toString());

  final String localeName;

  static AppLocalizations? of(BuildContext context) {
    return Localizations.of<AppLocalizations>(context, AppLocalizations);
  }

  static const LocalizationsDelegate<AppLocalizations> delegate = _AppLocalizationsDelegate();

  /// A list of this localizations delegate along with the default localizations
  /// delegates.
  ///
  /// Returns a list of localizations delegates containing this delegate along with
  /// GlobalMaterialLocalizations.delegate, GlobalCupertinoLocalizations.delegate,
  /// and GlobalWidgetsLocalizations.delegate.
  ///
  /// Additional delegates can be added by appending to this list in
  /// MaterialApp. This list does not have to be used at all if a custom list
  /// of delegates is preferred or required.
  static const List<LocalizationsDelegate<dynamic>> localizationsDelegates = <LocalizationsDelegate<dynamic>>[
    delegate,
    GlobalMaterialLocalizations.delegate,
    GlobalCupertinoLocalizations.delegate,
    GlobalWidgetsLocalizations.delegate,
  ];

  /// A list of this localizations delegate's supported locales.
  static const List<Locale> supportedLocales = <Locale>[
    Locale('en'),
    Locale('es'),
    Locale('pt')
  ];

  /// No description provided for @about.
  ///
  /// In en, this message translates to:
  /// **'About'**
  String get about;

  /// No description provided for @aboutDescription.
  ///
  /// In en, this message translates to:
  /// **'The current app works as a showcase for some of the most common features on developed apps.'**
  String get aboutDescription;

  /// No description provided for @aboutFeatures.
  ///
  /// In en, this message translates to:
  /// **'This app includes the next features:'**
  String get aboutFeatures;

  /// No description provided for @aboutWelcome.
  ///
  /// In en, this message translates to:
  /// **'Thank you for downloading this app demo.'**
  String get aboutWelcome;

  /// No description provided for @actionOk.
  ///
  /// In en, this message translates to:
  /// **'OK'**
  String get actionOk;

  /// No description provided for @actionReturnHome.
  ///
  /// In en, this message translates to:
  /// **'Return to home'**
  String get actionReturnHome;

  /// No description provided for @actionShare.
  ///
  /// In en, this message translates to:
  /// **'Share'**
  String get actionShare;

  /// No description provided for @actionSignIn.
  ///
  /// In en, this message translates to:
  /// **'Sign in'**
  String get actionSignIn;

  /// No description provided for @actionSignUp.
  ///
  /// In en, this message translates to:
  /// **'Sign up'**
  String get actionSignUp;

  /// No description provided for @appName.
  ///
  /// In en, this message translates to:
  /// **'AppDemo'**
  String get appName;

  /// No description provided for @birthday.
  ///
  /// In en, this message translates to:
  /// **'Birthday'**
  String get birthday;

  /// No description provided for @birthdayHint.
  ///
  /// In en, this message translates to:
  /// **'MM/DD/YYYY'**
  String get birthdayHint;

  /// No description provided for @country.
  ///
  /// In en, this message translates to:
  /// **'Country'**
  String get country;

  /// No description provided for @dateFormat.
  ///
  /// In en, this message translates to:
  /// **'MMM dd, yyyy'**
  String get dateFormat;

  /// No description provided for @email.
  ///
  /// In en, this message translates to:
  /// **'E-mail'**
  String get email;

  /// No description provided for @emailHint.
  ///
  /// In en, this message translates to:
  /// **'jsmith@example.com'**
  String get emailHint;

  /// No description provided for @emailInvalidError.
  ///
  /// In en, this message translates to:
  /// **'This field is not in a valid email format.'**
  String get emailInvalidError;

  /// No description provided for @error.
  ///
  /// In en, this message translates to:
  /// **'Error'**
  String get error;

  /// No description provided for @featureApi.
  ///
  /// In en, this message translates to:
  /// **'Rest API consuming & JSON processing.'**
  String get featureApi;

  /// No description provided for @featureFieldValidation.
  ///
  /// In en, this message translates to:
  /// **'Field validation.'**
  String get featureFieldValidation;

  /// No description provided for @featureFirebaseCore.
  ///
  /// In en, this message translates to:
  /// **'Firebase Analytics & Crashlytics integration.'**
  String get featureFirebaseCore;

  /// No description provided for @featureFirebaseFirestore.
  ///
  /// In en, this message translates to:
  /// **'Firebase Cloud Firestore data consuming.'**
  String get featureFirebaseFirestore;

  /// No description provided for @featureFirebaseMessaging.
  ///
  /// In en, this message translates to:
  /// **'Firebase Cloud Messaging integration for push notifications.'**
  String get featureFirebaseMessaging;

  /// No description provided for @featureLocalization.
  ///
  /// In en, this message translates to:
  /// **'Localization on multiple languages.'**
  String get featureLocalization;

  /// No description provided for @featureLocalPersistence.
  ///
  /// In en, this message translates to:
  /// **'Local persistence.'**
  String get featureLocalPersistence;

  /// No description provided for @featureResponsiveness.
  ///
  /// In en, this message translates to:
  /// **'Multiplatform responsiveness.'**
  String get featureResponsiveness;

  /// No description provided for @fieldEmptyError.
  ///
  /// In en, this message translates to:
  /// **'This field must not be empty.'**
  String get fieldEmptyError;

  /// No description provided for @fullName.
  ///
  /// In en, this message translates to:
  /// **'Full name'**
  String get fullName;

  /// No description provided for @fullNameHint.
  ///
  /// In en, this message translates to:
  /// **'John Smith'**
  String get fullNameHint;

  /// No description provided for @notifications.
  ///
  /// In en, this message translates to:
  /// **'Notifications'**
  String get notifications;

  /// No description provided for @notificationsEmptyError.
  ///
  /// In en, this message translates to:
  /// **'No notifications available'**
  String get notificationsEmptyError;

  /// No description provided for @password.
  ///
  /// In en, this message translates to:
  /// **'Password'**
  String get password;

  /// No description provided for @passwordConfirm.
  ///
  /// In en, this message translates to:
  /// **'Confirm password'**
  String get passwordConfirm;

  /// No description provided for @passwordHint.
  ///
  /// In en, this message translates to:
  /// **'******'**
  String get passwordHint;

  /// No description provided for @passwordMatchError.
  ///
  /// In en, this message translates to:
  /// **'The passwords doesn\'t match.'**
  String get passwordMatchError;

  /// No description provided for @products.
  ///
  /// In en, this message translates to:
  /// **'Products'**
  String get products;

  /// No description provided for @productsEmptyError.
  ///
  /// In en, this message translates to:
  /// **'No products available'**
  String get productsEmptyError;

  /// No description provided for @profile.
  ///
  /// In en, this message translates to:
  /// **'Profile'**
  String get profile;

  /// No description provided for @signIn.
  ///
  /// In en, this message translates to:
  /// **'Sign in'**
  String get signIn;

  /// No description provided for @signInBenefits.
  ///
  /// In en, this message translates to:
  /// **'Sign in now to take advantage of our community benefits!'**
  String get signInBenefits;

  /// No description provided for @signUp.
  ///
  /// In en, this message translates to:
  /// **'Sign up'**
  String get signUp;

  /// No description provided for @webNotFoundError.
  ///
  /// In en, this message translates to:
  /// **'404 Not Found'**
  String get webNotFoundError;

  /// No description provided for @webNotFoundErrorDetails.
  ///
  /// In en, this message translates to:
  /// **'Sorry, we couldn\'t find the page you where looking for.'**
  String get webNotFoundErrorDetails;
}

class _AppLocalizationsDelegate extends LocalizationsDelegate<AppLocalizations> {
  const _AppLocalizationsDelegate();

  @override
  Future<AppLocalizations> load(Locale locale) {
    return SynchronousFuture<AppLocalizations>(lookupAppLocalizations(locale));
  }

  @override
  bool isSupported(Locale locale) => <String>['en', 'es', 'pt'].contains(locale.languageCode);

  @override
  bool shouldReload(_AppLocalizationsDelegate old) => false;
}

AppLocalizations lookupAppLocalizations(Locale locale) {


  // Lookup logic when only language code is specified.
  switch (locale.languageCode) {
    case 'en': return AppLocalizationsEn();
    case 'es': return AppLocalizationsEs();
    case 'pt': return AppLocalizationsPt();
  }

  throw FlutterError(
    'AppLocalizations.delegate failed to load unsupported locale "$locale". This is likely '
    'an issue with the localizations generation tool. Please file an issue '
    'on GitHub with a reproducible sample app and the gen-l10n configuration '
    'that was used.'
  );
}
