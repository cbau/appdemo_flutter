import 'app_localizations.g.dart';

// ignore_for_file: type=lint

/// The translations for Portuguese (`pt`).
class AppLocalizationsPt extends AppLocalizations {
  AppLocalizationsPt([String locale = 'pt']) : super(locale);

  @override
  String get about => 'Sobre';

  @override
  String get aboutDescription => 'O aplicativo atual funciona como uma demonstração de alguns dos recursos mais comuns em aplicativos desenvolvidos.';

  @override
  String get aboutFeatures => 'Este aplicativo inclui os seguintes recursos:';

  @override
  String get aboutWelcome => 'Obrigado por baixar este app demo.';

  @override
  String get actionOk => 'OK';

  @override
  String get actionReturnHome => 'Retornar a página inicial';

  @override
  String get actionShare => 'Compartilhar';

  @override
  String get actionSignIn => 'Conecte-se';

  @override
  String get actionSignUp => 'Inscrever-se';

  @override
  String get appName => 'AppDemo';

  @override
  String get birthday => 'Data de nascimento';

  @override
  String get birthdayHint => 'DD/MM/YYYY';

  @override
  String get country => 'País';

  @override
  String get dateFormat => 'MMM dd, yyyy';

  @override
  String get email => 'Correio eletrônico';

  @override
  String get emailHint => 'jsilva@example.com';

  @override
  String get emailInvalidError => 'Este campo não possui um formato de email válido.';

  @override
  String get error => 'Erro';

  @override
  String get featureApi => 'Consumo de Rest API e processamento JSON.';

  @override
  String get featureFieldValidation => 'Validação de campo.';

  @override
  String get featureFirebaseCore => 'Integração do Firebase Analytics e Crashlytics.';

  @override
  String get featureFirebaseFirestore => 'Consumo de dados do Firebase Cloud Firestore.';

  @override
  String get featureFirebaseMessaging => 'Integração do Firebase Cloud Messaging para notificações push.';

  @override
  String get featureLocalization => 'Localização para vários idiomas.';

  @override
  String get featureLocalPersistence => 'Persistência local.';

  @override
  String get featureResponsiveness => 'Responsividade multiplataforma.';

  @override
  String get fieldEmptyError => 'Este campo não deve estar vazio.';

  @override
  String get fullName => 'Nome completo';

  @override
  String get fullNameHint => 'João Silva';

  @override
  String get notifications => 'Notificações';

  @override
  String get notificationsEmptyError => 'Nenhuma notificação disponível';

  @override
  String get password => 'Senha';

  @override
  String get passwordConfirm => 'Confirmar senha';

  @override
  String get passwordHint => '******';

  @override
  String get passwordMatchError => 'As senhas não coincidem.';

  @override
  String get products => 'Produtos';

  @override
  String get productsEmptyError => 'Nenhum produto disponível';

  @override
  String get profile => 'Perfil';

  @override
  String get signIn => 'Conecte-se';

  @override
  String get signInBenefits => 'Cadastre-se agora para aproveitar os benefícios de nossa comunidade!';

  @override
  String get signUp => 'Inscrever-se';

  @override
  String get webNotFoundError => '404 Não Encontrado';

  @override
  String get webNotFoundErrorDetails => 'Não foi possível encontrar a página que você estava procurando.';
}
