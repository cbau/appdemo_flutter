/// The build configuration.
// ignore_for_file: do_not_use_environment
class BuildConfig {
  /// The current flavor of the application.
  static const flavor = String.fromEnvironment('flavor', defaultValue: 'dev');
}
