import 'package:flutter/foundation.dart';
import 'package:path/path.dart';
import 'package:path_provider/path_provider.dart';
import 'package:sembast/sembast_io.dart';
import 'package:sembast/sembast_memory.dart';
import 'package:sembast_web/sembast_web.dart';

/// The local service.
class LocalService {
  /// Factory instance constructor.
  factory LocalService() => _instance;

  /// Private constructor for this class.
  LocalService._();

  static final _instance = LocalService._();

  /// The notification store.
  final notificationStore = intMapStoreFactory.store('notifications');

  /// The preference store.
  final preferenceStore = StoreRef.main();

  /// The product store.
  final productStore = intMapStoreFactory.store('products');

  Database? _database;

  /// When set to true, the database will be opened from a volatile a space in
  /// memory instead. Great for testing purposes.
  bool openDatabaseFromMemory = false;

  /// The local database.
  Future<Database> get database async => _database ??= await _createDatabase();

  /// Clears all the local data.
  Future<void> clearAll({bool keepPreferences = false}) async {
    final db = await database;
    await notificationStore.delete(db);
    await productStore.delete(db);

    if (!keepPreferences) {
      await preferenceStore.delete(db);
    }
  }

  Future<Database> _createDatabase() async {
    const fileName = 'appdb.db';

    if (openDatabaseFromMemory) {
      return databaseFactoryMemory.openDatabase(fileName);
    }

    // coverage:ignore-start
    if (kIsWeb) {
      return databaseFactoryWeb.openDatabase(fileName);
    }

    final dir = await getApplicationDocumentsDirectory();
    await dir.create(recursive: true);
    final path = join(dir.path, fileName);
    return databaseFactoryIo.openDatabase(path);
    // coverage:ignore-end
  }
}
