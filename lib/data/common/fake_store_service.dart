import 'package:http/http.dart';

/// The fake store service.
class FakeStoreService {
  /// Factory instance constructor.
  factory FakeStoreService() => _instance;

  /// Private constructor for this class.
  FakeStoreService._();

  static final FakeStoreService _instance = FakeStoreService._();

  Client _client = Client();

  /// The service origin.
  String get serviceOrigin => 'https://fakestoreapi.com';

  /// Sets the client used by this service.
  // ignore: use_setters_to_change_properties
  void setClient(Client client) => _client = client;

  /// Gets a response for the products on this service.
  Future<Response> getProducts() =>
      _client.get(Uri.parse('$serviceOrigin/products'));

  /// Gets a response for the product of the requested identifier
  /// on this service.
  Future<Response> getProduct(int id) =>
      _client.get(Uri.parse('$serviceOrigin/products/$id'));
}
