/// An HTTP status exception.
class HttpStatusException implements Exception {
  /// Initializes the [statusCode] and [reasonPhrase] for this
  /// [HttpStatusException].
  HttpStatusException(
    this.statusCode, {
    this.reasonPhrase,
  });

  /// The status code for this exception.
  final int statusCode;

  /// The reason phrase for this exception.
  final String? reasonPhrase;

  @override
  String toString() => 'HTTP Status Exception: $statusCode $reasonPhrase';
}
