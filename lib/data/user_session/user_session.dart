/// An user session.
class UserSession {
  /// Initializes the properties of this [UserSession].
  const UserSession({
    required this.birthday,
    required this.country,
    required this.email,
    required this.name,
  });

  /// The user birthday.
  final DateTime birthday;

  /// The user country.
  final String country;

  /// The user email.
  final String email;

  /// The user name.
  final String name;
}
