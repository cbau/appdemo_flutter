import 'user_session.dart';

/// The user session repository.
class UserSessionRepository {
  /// Factory instance constructor.
  factory UserSessionRepository() => _instance;

  /// Private constructor for this class.
  const UserSessionRepository._();

  static const _instance = UserSessionRepository._();

  /// Signs in the user with a [username] and a [password].
  Future<void> signIn({
    required String username,
    required String password,
  }) async {
    // Simulate server connection. Implement future connection here.
    await Future<void>.delayed(const Duration(seconds: 1));
    throw Exception('Incorrect email or password. Please verify your data.');
  }

  /// Signs up a [user] with a [password].
  Future<void> signUp({
    required UserSession user,
    required String password,
  }) async {
    // Simulate server connection. Implement future connection here.
    await Future<void>.delayed(const Duration(seconds: 1));
    throw Exception('Problem connecting to the server. Please try later.');
  }
}
