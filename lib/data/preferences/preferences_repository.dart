import 'dart:async';

import 'package:sembast/sembast.dart';

import '../common/local_service.dart';

/// The preferences repository.
class PreferencesRepository {
  /// Factory instance constructor.
  factory PreferencesRepository() => _instance;

  /// Private constructor for this class.
  PreferencesRepository._() {
    unawaited(_init());
  }

  static final _instance = PreferencesRepository._();
  final _store = LocalService().preferenceStore;
  late final Database _database;
  var _notificationsLastSync = 0;
  var _productsLastSync = 0;

  /// The last time notifications were syncronized.
  int get notificationsLastSync => _notificationsLastSync;

  set notificationsLastSync(int value) {
    _notificationsLastSync = value;
    unawaited(_store.record('notificationsLastSync').put(_database, value));
  }

  /// The last time products were syncronized.
  int get productsLastSync => _productsLastSync;

  set productsLastSync(int value) {
    _productsLastSync = value;
    unawaited(_store.record('productsLastSync').put(_database, value));
  }

  Future<void> _init() async {
    _database = await LocalService().database;
    _notificationsLastSync =
        await _store.record('notificationsLastSync').get(_database) as int? ??
            0;
    _productsLastSync =
        await _store.record('productsLastSync').get(_database) as int? ?? 0;
  }
}
