import 'package:sembast/sembast.dart';

import '../common/local_service.dart';
import 'notification.dart';

/// The notification local data source.
class NotificationLocalDataSource {
  /// Factory instance constructor.
  factory NotificationLocalDataSource() => _instance;

  /// Private constructor for this class.
  const NotificationLocalDataSource._();

  static const _instance = NotificationLocalDataSource._();

  /// Gets all the items from this data source.
  Future<List<Notification>> get all async {
    final database = await LocalService().database;
    final store = LocalService().notificationStore;
    final snapshot = await store.query().getSnapshots(database);
    return snapshot.map((e) => Notification.fromMap(e.value)).toList();
  }

  /// Adds the [item] to this data source.
  Future<void> add(Notification item) async {
    final database = await LocalService().database;
    final store = LocalService().notificationStore;
    await store.add(database, item.toMap());
  }

  /// Adds all the specificed [items] to this data source.
  Future<void> addAll(List<Notification> items) async {
    for (final item in items) {
      await add(item);
    }
  }

  /// Clears all the items from this data source.
  Future<void> clear() async {
    final database = await LocalService().database;
    final store = LocalService().notificationStore;
    await store.delete(database);
  }
}
