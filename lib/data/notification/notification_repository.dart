import 'package:flutter/foundation.dart';

import '../preferences/preferences_repository.dart';
import 'notification.dart';
import 'notification_local_data_source.dart';
import 'notification_remote_data_source.dart';

/// The notification repository.
class NotificationRepository {
  /// Factory instance constructor.
  factory NotificationRepository() => _instance;

  /// Private constructor for this class.
  const NotificationRepository._();

  static const _instance = NotificationRepository._();

  /// Gets all the notifications.
  Stream<List<Notification>> getAll({
    bool force = false,
  }) async* {
    final items = await NotificationLocalDataSource().all;
    if (items.isNotEmpty) {
      yield items;
    }

    final nextSync = PreferencesRepository().notificationsLastSync -
        DateTime.now().add(const Duration(minutes: -5)).millisecondsSinceEpoch;
    if (force || nextSync < 0) {
      final items = await NotificationRemoteDataSource().all;
      await NotificationLocalDataSource().clear();
      await NotificationLocalDataSource().addAll(items);
      PreferencesRepository().notificationsLastSync =
          DateTime.now().millisecondsSinceEpoch;

      yield items;
    } else {
      debugPrint('Local data is fresh. Next refresh will be in ${nextSync}ms');
      if (items.isEmpty) {
        yield [];
      }
    }
  }
}
