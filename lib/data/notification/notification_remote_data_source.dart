import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:flutter/foundation.dart';

import 'notification.dart';

/// The notification remote data source.
class NotificationRemoteDataSource {
  /// Factory instance constructor.
  factory NotificationRemoteDataSource() => _instance;

  /// Private constructor for this class.
  NotificationRemoteDataSource._();

  static final _instance = NotificationRemoteDataSource._();

  FirebaseFirestore? _firestore;

  @visibleForTesting

  /// Sets the Firestore instance to be used.
  // ignore: avoid_setters_without_getters
  set firestore(FirebaseFirestore value) => _firestore = value;

  /// Gets all the items from this data source.
  Future<List<Notification>> get all async {
    _firestore ??= FirebaseFirestore.instance;
    final snapshot = await _firestore!.collection('notifications').get();
    final items =
        snapshot.docs.map((e) => Notification.fromMap(e.data())).toList();
    return items;
  }
}
