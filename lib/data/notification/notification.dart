import 'package:cloud_firestore/cloud_firestore.dart';

import '../../extensions.dart';

/// A notification.
class Notification {
  /// Initializes the properties for this [Notification].
  const Notification({
    required this.message,
    required this.timestamp,
    required this.title,
  });

  /// Initializes the [Notification] from a [Map].
  Notification.fromMap(Map<String, Object?>? map)
      : this(
          message: map?['message']?.tryCast<String>() ?? '',
          timestamp: map?['timestamp']?.tryCast<Timestamp>()?.toDate() ??
              DateTime.fromMillisecondsSinceEpoch(
                map?['timestamp']?.tryCast<int>() ?? 0,
              ),
          title: map?['title']?.tryCast<String>() ?? '',
        );

  /// The message of this notification.
  final String message;

  /// The timestamp when this notification was created.
  final DateTime timestamp;

  /// The title of this notification.
  final String title;

  /// Converts this [Notification] to a [Map].
  Map<String, Object?> toMap() => {
        'message': message,
        'timestamp': timestamp.millisecondsSinceEpoch,
        'title': title,
      };
}
