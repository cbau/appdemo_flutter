import 'package:flutter/foundation.dart';

import '../../extensions.dart';

/// A rating.
@immutable
class Rating {
  /// Initializes the properties of this [Rating].
  const Rating({
    this.count = 0,
    this.rate = 0.0,
  });

  /// Initializes the [Rating] from a [Map].
  Rating.fromMap(Map<String, Object?>? map)
      : this(
          count: map?['count']?.tryCast<int>() ?? 0,
          rate: map?['rate']?.tryCast<double>() ?? 0.0,
        );

  /// The total of ratings.
  final int count;

  /// The average rating.
  final double rate;

  /// Converts this [Rating] to a [Map].
  Map<String, Object?> toMap() => {
        'count': count,
        'rate': rate,
      };

  @override
  bool operator ==(covariant Rating other) =>
      identical(this, other) || other.count == count && other.rate == rate;

  @override
  int get hashCode => count.hashCode ^ rate.hashCode;

  /// Creates a copy of this model.
  Rating copyWith({
    int? count,
    double? rate,
  }) =>
      Rating(
        count: count ?? this.count,
        rate: rate ?? this.rate,
      );

  @override
  String toString() => 'Rating(count: $count, rate: $rate)';
}
