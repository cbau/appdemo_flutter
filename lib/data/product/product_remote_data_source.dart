import 'dart:convert';

import '../common/fake_store_service.dart';
import '../common/http_status_exception.dart';
import 'product.dart';

/// The product remote data source.
class ProductRemoteDataSource {
  /// Factory instance constructor.
  factory ProductRemoteDataSource() => _instance;

  /// Private constructor for this class.
  const ProductRemoteDataSource._();

  static const _instance = ProductRemoteDataSource._();

  /// Gets all the items from this data source.
  Future<List<Product>> get all async {
    final response = await FakeStoreService().getProducts();

    if (response.statusCode >= 300) {
      throw HttpStatusException(
        response.statusCode,
        reasonPhrase: response.reasonPhrase,
      );
    }

    final jsonReponse = json.decode(response.body);
    if (jsonReponse is! List) {
      throw Exception(
        'Expected List in response of type ${jsonReponse.runtimeType}',
      );
    }

    final items = jsonReponse
        .map((e) => Product.fromMap(e as Map<String, Object?>?))
        .toList();

    return items;
  }

  /// Gets an item from this data source by its identifier.
  Future<Product> getById(int id) async {
    final response = await FakeStoreService().getProduct(id);

    if (response.statusCode >= 300) {
      throw HttpStatusException(
        response.statusCode,
        reasonPhrase: response.reasonPhrase,
      );
    }

    final jsonReponse = json.decode(response.body);
    if (jsonReponse is! Map<String, Object?>?) {
      throw Exception(
        'Expected Map in response of type ${jsonReponse.runtimeType}',
      );
    }

    return Product.fromMap(jsonReponse);
  }
}
