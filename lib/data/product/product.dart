import 'package:flutter/foundation.dart';

import '../../extensions.dart';
import 'rating.dart';

/// A product.
@immutable
class Product {
  /// Initializes the properties of this [Product].
  const Product({
    required this.id,
    this.category = '',
    this.description = '',
    this.image = '',
    this.price = 0.0,
    this.rating = const Rating(),
    this.title = '',
    this.url = '',
  });

  /// Initializes the [Product] from a [Map].
  Product.fromMap(Map<String, Object?>? map)
      : this(
          category: map?['category']?.tryCast<String>() ?? '',
          description: map?['description']?.tryCast<String>() ?? '',
          id: map?['id']?.tryCast<int>() ?? 0,
          image: map?['image']?.tryCast<String>() ?? '',
          price: map?['price']?.tryCast<double>() ?? 0.0,
          rating:
              Rating.fromMap(map?['rating']?.tryCast<Map<String, Object?>>()),
          title: map?['title']?.tryCast<String>() ?? '',
          url:
              'https://cbau.gitlab.io/appdemo_flutter/products/${map?['id']?.tryCast<int>() ?? 0}',
        );

  /// The product category.
  final String category;

  /// The product description.
  final String description;

  /// The product identifier.
  final int id;

  /// The product image URL.
  final String image;

  /// The product price.
  final double price;

  /// The product rating.
  final Rating rating;

  /// The product title.
  final String title;

  /// The product URL.
  final String url;

  /// Converts this [Product] to a [Map].
  Map<String, Object?> toMap() => {
        'category': category,
        'description': description,
        'id': id,
        'image': image,
        'price': price,
        'rating': rating.toMap(),
        'title': title,
        'url': url,
      };

  @override
  bool operator ==(covariant Product other) =>
      identical(this, other) ||
      other.category == category &&
          other.description == description &&
          other.id == id &&
          other.image == image &&
          other.price == price &&
          other.rating == rating &&
          other.title == title &&
          other.url == url;

  @override
  int get hashCode =>
      category.hashCode ^
      description.hashCode ^
      id.hashCode ^
      image.hashCode ^
      price.hashCode ^
      rating.hashCode ^
      title.hashCode ^
      url.hashCode;

  /// Creates a copy of this model.
  Product copyWith({
    String? category,
    String? description,
    int? id,
    String? image,
    double? price,
    Rating? rating,
    String? title,
    String? url,
  }) =>
      Product(
        category: category ?? this.category,
        description: description ?? this.description,
        id: id ?? this.id,
        image: image ?? this.image,
        price: price ?? this.price,
        rating: rating ?? this.rating,
        title: title ?? this.title,
        url: url ?? this.url,
      );

  @override
  String toString() => 'Product('
      'category: $category, '
      'description: $description, '
      'id: $id, '
      'image: $image, '
      'price: $price, '
      'rating: $rating, '
      'title: $title, '
      'url: $url)';
}
