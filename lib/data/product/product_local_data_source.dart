import 'package:sembast/sembast.dart';

import '../common/local_service.dart';
import 'product.dart';

/// The product local data source.
class ProductLocalDataSource {
  /// Factory instance constructor.
  factory ProductLocalDataSource() => _instance;

  /// Private constructor for this class.
  const ProductLocalDataSource._();

  static const _instance = ProductLocalDataSource._();

  /// Gets all the items from this data source.
  Future<List<Product>> get all async {
    final database = await LocalService().database;
    final store = LocalService().productStore;
    final snapshot = await store.query().getSnapshots(database);
    return snapshot.map((e) => Product.fromMap(e.value)).toList();
  }

  /// Clears all the items from this data source.
  Future<void> clear() async {
    final database = await LocalService().database;
    final store = LocalService().productStore;
    await store.delete(database);
  }

  /// Deletes an item from this data source by its identifier.
  Future<void> deleteById(int id) async {
    final database = await LocalService().database;
    final store = LocalService().productStore;
    await store.record(id).delete(database);
  }

  /// Gets an item from this data source by its identifier.
  Future<Product?> getById(int id) async {
    final database = await LocalService().database;
    final store = LocalService().productStore;
    final map = await store.record(id).get(database);
    return map == null ? null : Product.fromMap(map);
  }

  /// Puts the [item] to this data source.
  Future<void> put(Product item) async {
    final database = await LocalService().database;
    final store = LocalService().productStore;
    await store.record(item.id).put(database, item.toMap());
  }

  /// Puts all the specificed [items] to this data source.
  Future<void> putAll(List<Product> items) async {
    for (final item in items) {
      await put(item);
    }
  }
}
