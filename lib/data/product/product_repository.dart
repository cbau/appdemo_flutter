import 'package:flutter/foundation.dart';

import '../preferences/preferences_repository.dart';
import 'product.dart';
import 'product_local_data_source.dart';
import 'product_remote_data_source.dart';

/// The product repository.
class ProductRepository {
  /// Factory instance constructor.
  factory ProductRepository() => _instance;

  /// Private constructor for this class.
  const ProductRepository._();

  static const _instance = ProductRepository._();

  /// Gets all the products.
  Stream<List<Product>> getAll({
    bool force = false,
  }) async* {
    final items = await ProductLocalDataSource().all;
    if (items.isNotEmpty) {
      yield items;
    }

    final nextSync = PreferencesRepository().productsLastSync -
        DateTime.now().add(const Duration(minutes: -60)).millisecondsSinceEpoch;
    if (force || nextSync < 0) {
      final items = await ProductRemoteDataSource().all;
      await ProductLocalDataSource().clear();
      await ProductLocalDataSource().putAll(items);
      PreferencesRepository().productsLastSync =
          DateTime.now().millisecondsSinceEpoch;

      yield items;
    } else {
      debugPrint('Local data is fresh. Next refresh will be in ${nextSync}ms');
      if (items.isEmpty) {
        yield [];
      }
    }
  }

  /// Gets a product by its identifier.
  Future<Product?> getById(int id) async {
    final item = await ProductLocalDataSource().getById(id);

    if (item == null) {
      return ProductRemoteDataSource().getById(id);
    }

    return item;
  }
}
